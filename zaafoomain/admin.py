from django.contrib import admin
from .models import *
class MenuAdmin(admin.ModelAdmin):
	list_display=['name','food_timings','restaurant','cuisine']
class VarAdmin(admin.ModelAdmin):
	list_display=['param','cvalue','bvalue','fvalue']
# Register your models here.
admin.site.register(Cities)
admin.site.register(Locality)
admin.site.register(States)
admin.site.register(Cuisine)
admin.site.register(Restaurant)
admin.site.register(Restaurant_Images)
admin.site.register(Food_Timings)
admin.site.register(RestaurantFoodTiming)
admin.site.register(Restaurant_Owner)
admin.site.register(Menu,MenuAdmin)
admin.site.register(Floor)
admin.site.register(FloorPlan)
admin.site.register(Tables)
admin.site.register(Profil)
admin.site.register(Booking)
admin.site.register(TableBooking)
admin.site.register(MenuBooking)
admin.site.register(CustomerFeedback)
admin.site.register(Transaction)
admin.site.register(BookingStatus)
admin.site.register(RestaurantDiscount)
admin.site.register(RestaurantTableBlock)
admin.site.register(RestaurantTax)
admin.site.register(ExtraBooking)
admin.site.register(ExtraMenuBooking)
admin.site.register(FloorTexts)
admin.site.register(UserExtra)
admin.site.register(RestaurantServiceCharge)
admin.site.register(RestaurantServiceTax)
admin.site.register(BillTax)
admin.site.register(ExtraBillTax)
admin.site.register(PromoEmailList)
admin.site.register(PromoPhnoList)
admin.site.register(VariableParams,VarAdmin)
admin.site.register(Cancellation)

