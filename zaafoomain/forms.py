from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms import ModelForm
from .models import *
from django.core.validators import *
from django.core.mail import send_mail
from django.template import Context,Template
from django.conf import settings
from django.core.mail import EmailMessage

from django.utils import html
import datetime
from django.utils import timezone
class LoginForm(AuthenticationForm):
    """Login form."""

    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(
            attrs={'class': 'mdl-textfield__input', 'name': 'username'}))
    password = forms.CharField(
        label="Password",
        max_length=30,
        widget=forms.PasswordInput(
            attrs={'class': 'mdl-textfield__input', 'name': 'password'}))
class MenuForm(ModelForm):
    """docstring for MenuForm"""
    class Meta:
        model=Menu
        fields=('name','description','food_timings','restaurant','Veg','cuisine','price')

class CustomerFeedbackForm(ModelForm):
    """docstring for MenuForm"""
    restaurant=forms.TextInput(attrs={'readonly':'readonly'})
    customer=forms.TextInput(attrs={'readonly':'readonly'})
    title=forms.TextInput()
    description=forms.TextInput()
    cRATINGS=((1,1),(2,2),(3,3),(4,4),(5,5))
    rating=forms.ChoiceField(choices=cRATINGS,label="Rating", required=True)
    class Meta:
        model=CustomerFeedback
        fields=('title','description','rating')

class BasketForm(forms.Form):
    rideschoices = forms.CharField(
        widget=forms.HiddenInput(
            attrs={
                'class': 'form-control',
                'name': 'rideschoices', 'value': '[]'}))
       


class RegistrationForm(forms.Form):
    username = forms.CharField(label="",widget=forms.TextInput(attrs={'placeholder': 'Username','class':'form-control input-perso'}),max_length=30,min_length=3)
    first_name= forms.CharField(label="", widget=forms.TextInput(
        attrs={'placeholder': 'First Name', 'class': 'form-control input-perso'}), max_length=30, min_length=3)
    last_name = forms.CharField(label="", widget=forms.TextInput(
        attrs={'placeholder': 'Last Name', 'class': 'form-control input-perso'}), max_length=30, min_length=3)
    email = forms.EmailField(label="",widget=forms.EmailInput(attrs={'placeholder': 'Email','class':'form-control input-perso'}),max_length=100,error_messages={'invalid': ("Email invalide.")})
    password1 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password','class':'form-control input-perso'}))
    password2 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password','class':'form-control input-perso'}))


    #recaptcha = ReCaptchaField()

    #Override of clean method for password check
    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            self._errors['password2'] = [u"Password don't match!"]

        return self.cleaned_data

    #Override of save method for saving both User and Profil objects
    def save(self, datas):
        u = User.objects.create_user(datas['username'],
                                     datas['email'],
                                     datas['password1'])
        u.is_active = False
        u.first_name=datas['first_name']
        u.last_name = datas['last_name']
        u.save()
        profil=Profil()
        profil.user=u
        profil.activation_key=datas['activation_key']
        profil.key_expires=datetime.datetime.strftime(timezone.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
        profil.save()
        return u

    #Handling of activation email sending ------>>>!! Warning : Domain name is hardcoded below !!<<<------
    #I am using a text file to write the email (I write my email in the text file with templatetags and then populate it with the method below)
    def sendEmail(self, datas):
        link="http://zaafoo.com/activate/"+datas['activation_key']
        c=Context({'activation_link':link,'username':datas['username']})
        f = open(settings.STATIC_PATH+datas['email_path'], 'r')
        t = Template(f.read())
        f.close()
        message=t.render(c)
        #print unicode(message).encode('utf8')
        send_mail(datas['email_subject'], message, 'zaafoonoreply@gmail.com', [datas['email']], fail_silently=False)
        email = EmailMessage(datas['email_subject'], 'message', [datas['email']])
        email.send()

class EditProfileForm(forms.ModelForm):

    first_name = forms.CharField(label='First Name')
    last_name = forms.CharField(label='Last Name')

    class Meta:
        model = User
        fields = ['first_name', 'last_name']

class ChangePasswordForm(forms.Form):
    password1 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password','class':'form-control input-perso'}))
    password2 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password','class':'form-control input-perso'}))

    #recaptcha = ReCaptchaField()

    #Override of clean method for password check
    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password1 != password2:
            self._errors['password2'] = [u"Password don't match!"]

        return self.cleaned_data
