from zaafoomain.views import restaurantlogin
from .models import *

def taxtables():
	res=Restaurant.objects.all()
	for r in res:
		servicecharges = RestaurantServiceCharge.objects.filter(restaurant=r)
		resdis = RestaurantDiscount.objects.filter(restaurant=r)
		taxes = RestaurantTax.objects.filter(restaurant=r)
		service_taxesgen = RestaurantServiceTax.objects.filter(restaurant=r, name="net").first()

		if len(servicecharges) == 0:
			servicecharges = RestaurantServiceCharge(restaurant=r, pc=0.05, name="Service Charge")
			servicecharges.save()
		if len(resdis) == 0:
			resdis = RestaurantDiscount(restaurant=r, minprice=300, discountpc=0.1,
										tablebookingwaivermin=3000.0)
			resdis.save()
		if len(taxes) == 0:
			taxes = RestaurantTax(restaurant=r, pc=0.15, name="VAT")
			taxes.save()
		if service_taxesgen is None:
			service_taxesgen = RestaurantServiceTax(restaurant=r, pc=0.15, name="net")
			service_taxesgen.save()

	return
