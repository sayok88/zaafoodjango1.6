from django.conf.urls import url,include
from . import views,testview,viewsm,accountview,promo_rest
from django.views.generic.base import TemplateView
from django.contrib.staticfiles.urls import static,staticfiles_urlpatterns
from django.conf import settings
from rest_framework import routers

router = routers.DefaultRouter()
#router.register(r'users', views.UserViewSet)

router.register(r'cities', views.CityViewSet)

# We are adding a URL called /home
urlpatterns = [
    url(r'^dashboard/', views.dashboard, name='dashboard'),
    url(r'^dashboard2/', views.dashboard2, name='dashboard2'),
    url(r'^tos/', views.tos, name='Terms'),
    url(r'^aboutus/', views.aboutus, name='AboutUs'),
    url(r'^Contracts/', views.contracts, name='Contracts'),
    url(r'^CancellaetionandRefunds/', views.cancelrefund, name='Contracts'),
    url(r'^respastbook/(\d+)', views.respastbook, name='Past Bookings'),
    url(r'^rescurrbook/(\d+)', views.rescurrbook, name='Current Bookings'),
    url(r'^rescanbook/(\d+)', views.rescanbook, name='Cancelled Bookings'),
    url(r'^customerfeedback/(\d+)', views.customerfeedback, name='customerfeedback'),
    url(r'^resbill/(\d+)', views.resbill, name='Current Bookings'),
    url(r'^ebill/', views.ebill, name='Ebill'),
    url(r'^contactus/', views.contactus, name='Contact-us'),
    url(r'^careers/', views.careers, name='Careers'),
    url(r'^pp/', views.pp, name='Privacy Policy'),
    url(r'^restaurantlogin/', views.restaurantlogin, name='restaurantlogin'),
    url(r'^createMenu/', views.createMenu, name='createMenu'),
    url(r'^cuslogin/', views.cuslogin, name='cuslogin'),
    url(r'^logout_view/', views.logout_view, name='logout_view'),
    url(r'^reslist/', views.reslist, name='reslist'),
    url(r'^basket/', views.basket, name='basket'),
    url(r'^ownerMenuList/(\d+)', views.ownerMenuList, name='ownerMenuList'),
    url(r'^cusResBook/(\d+)', views.cusResBook, name='cusResBook'),
    url(r'^feedback/(\d+)', views.resfeedbacklist, name='resfeedbacklist'),
    url(r'^mycustomers/(\d+)', views.mycustomers, name='mycustomers'),
    url(r'^deletemenu/(\d+)', views.deletemenu, name='deletemenu'),
    url(r'^ownerTableBooking/(\d+)', views.ownerTableBooking, name='ownerTableBooking'),
    url(r'^register/$', views.register,name='register'),
	url(r'^activate/(?P<key>.+)$',views.activation ,name='activation'),
	url(r'^new-activation-link/(?P<user_id>\d+)/$', views.new_activation_link ,name='new_activation_link'),
    url(r'^$', views.home, name='home'),
    url(r'^publicrest/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^loginrest/', testview.LoginView.as_view(), name='login-view'),
    url(r'^registrationrest/', testview.RegistrationView.as_view(), name='registration-view'),
    url(r'^restcity/', testview.CityRestaurants.as_view(), name='cityrest-view'),
    url(r'^restlocal/', testview.LocalRestaurants.as_view(), name='locrest-view'),
    url(r'^cusresview/', testview.RestaurantMenuandTable.as_view(), name='cusrest-view'),
    url(r'^resbookview/', testview.BookRestaurant.as_view(), name='bookrest-view'),
    url(r'^contranview/', testview.ConfirmTransaction.as_view(), name='contran-view'),
    url(r'^useremailview/', testview.GetUSerEmail.as_view(), name='GetUSerEmail-view'),
    url(r'^givefeedbackview/', testview.GiveFeedback.as_view(), name='GetUSerEmail-view'),
    url(r'^getfeedbackview/', testview.GetFeedback.as_view(), name='GetUSerEmail-view'),
    url(r'^fetchRestaurantBookings/', testview.FetchRestrauntBookings.as_view(), name='Fetch-Restaurant-Bookings'),
    url(r'^auth/', testview.AuthView.as_view(), name='auth-view'),
    url(r'^forgotpasswordview/', testview.ForgotPassword.as_view(), name='Forgot-Password-view'),
    url(r'^changepasswordview/', testview.ChangePassword.as_view(), name='Change-Password-view'),
    url(r'^localityview/', testview.Localities.as_view(), name='Localities-view'),
    url(r'^Disclaimer/', views.disclaimer, name='Disclaimer-view'),
    url(r'^mob/', views.mob, name='mobile-view'),
    url(r'^mobverifyview/', testview.VerifyMobile.as_view(), name='verifymobile-view'),
    url(r'^calculatebillview/', testview.CalculateBill.as_view(), name='calculate-bill-view'),
    #m view
    url(r'^m/cuslogin/', viewsm.cuslogin, name='cuslogin'),
    url(r'^m/logout_view/', viewsm.logout_view, name='logout_view'),
    url(r'^m/reslist/', viewsm.reslist, name='reslist'),
    url(r'^m/basket/', viewsm.basket, name='basket'),
    url(r'^m/ownerMenuList/(\d+)', viewsm.ownerMenuList, name='ownerMenuList'),
    url(r'^m/cusResBook/(\d+)', viewsm.cusResBook, name='cusResBook'),
    url(r'^m/mycustomers/(\d+)', viewsm.mycustomers, name='mycustomers'),
    url(r'^m/deletemenu/(\d+)', viewsm.deletemenu, name='deletemenu'),
    url(r'^m/register/$', viewsm.register, name='register'),
    url(r'^m/$', viewsm.home, name='home'),
    url(r'^facebook-signup/$', testview.FacebookLoginOrSignup.as_view(), name='facebook-login-signup'),
    url(r'^mytransactionsview/$', testview.MyTransactions.as_view(), name='facebook-login-signup'),
    url(r'^profile/$', accountview.profile_view, name='Profile'),
    url(r'^editprofile/$', accountview.edit_profile, name='Edit_Profile'),
    url(r'^changepassword/$', accountview.change_password, name='Change_Password'),
    url(r'^cuspastbookings/$', accountview.MyTransactions, name='My-Transactions'),
    url(r'^cuscurrbookings/$', accountview.cuscurrbook, name='My-Upcoming-Bookings'),
    url(r'^promo/$', promo_rest.PromoOn.as_view(), name='promo-on'),
    url(r'^storeemailphno/$', promo_rest.StoreEmailPhno.as_view(), name='StoreEmailPhno'),
    url(r'^getpromobookingremaining/$', promo_rest.GetPromoBookingRemaining.as_view(), name='GetPromoBookingRemaining'),
    url(r'^varparamread/$', promo_rest.VarParamRead.as_view(), name='ParamRead'),
    url(r'^cancelbooking/(\d+)', accountview.cancelbooking, name='cancelbooking'),
    url(r'^cancelations/$', accountview.cancelations, name='cancelations'),
    url(r'^cancelledbill/(\d+)', accountview.cancelledbill, name='cancelledbill'),
    url(r'^bookingcancelrest/', testview.CancelBooking.as_view(), name='cancelledbill_api'),
    url(r'^api/get_drugs/(\d+)', views.get_drugs, name='get_drugs'),
    url(r'^test/$',views.test,name='test'),

]
#urlpatterns += staticfiles_urlpatterns() #this serves static files and media files.
    #in case media is not served correctly
#urlpatterns += patterns('',
#        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
#            'document_root': settings.MEDIA_ROOT,
#            }),
#    )
urlpatterns += staticfiles_urlpatterns()
#urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
