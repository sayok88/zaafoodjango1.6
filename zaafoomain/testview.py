from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.db.models import Avg
from rest_framework import HTTP_HEADER_ENCODING, exceptions
from rest_framework.decorators import permission_classes
from allauth.socialaccount import providers
from allauth.socialaccount.models import SocialLogin, SocialToken, SocialApp
from allauth.socialaccount.providers.facebook.views import fb_complete_login
from allauth.socialaccount.helpers import complete_social_login
import hashlib,random
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework import status 
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.core import serializers
import dateutil.parser
import json
from .forms import *
from django.contrib.auth import authenticate
from .models import *
from .forms import *
from django.forms.models import model_to_dict
from .serializers import EverybodyCanAuthentication
from django.core.serializers.json import DjangoJSONEncoder
from  .zflib import  *
# Create your views here.
import requests
class LoginView(APIView):
	"""
	"""

	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):

		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		print(data)
		print(request.POST)
		try:
			print('datac')
			datac = json.loads(request.data)
			print(datac)
			userc = datac['user']
			print(userc)
			print(QueryDict(request.body).get('user'))
		except:
			print("error")
		try:
			datastr = str(data)
			print('datastr')
			print(datastr)
			print(data[' name'])
			print(datastr.find('\\r'))
		except:
			print("error")
		if "user" not in data or "password" not in data:
			return Response(
				'parameters mising',
				status=status.HTTP_401_UNAUTHORIZED
			)
		username = request.data['user']
		password = request.data['password']

		print(username)
		""" Exception block req """
		user2 = None
		user=None
		try:
			user2 = User.objects.get(username=username)
		except:
			print('no user, should exit here')
		user = authenticate(username=username, password=password)
		if user is None and user2 is not None:
			user = authenticate(username=user2.email, password=password)
		if user is None:
			return Response(
				'No default user, please create one',
				status=status.HTTP_404_NOT_FOUND
			)
		userext = None
		mobile=False
		try:
			userext = UserExtra.objects.get(user=user)
			if userext.isMobileVerified:
				mobile = userext.isMobileVerified
		except Exception as e:
			print(e)

		token = Token.objects.get_or_create(user=user)
		return Response({'detail': 'POST answer', 'token': token[0].key,'isMobileVerified':mobile})

class ForgotPassword(APIView):
	"""
	"""

	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		if "email" not in data:
			return Response(
				'parameters mising',
				status=status.HTTP_401_UNAUTHORIZED
			)
		email = request.data['email']
		print(email)
		""" Exception block req """
		user = User.objects.get(email=email)
		if user is None:
			return Response(
				'Email does not exist',
				status=status.HTTP_404_NOT_FOUND
			)
		datas = {}
		datas['email'] = email

		# We will generate a random activation key
		salt = hashlib.sha1(str(random.random())).hexdigest()[:10]
		datas['password'] = salt

		datas['email_path'] = "/NewPasswordEmail.txt"
		datas['email_subject'] = "[zaafoo] Your Auto generated Password "

		self.sendEmail(datas)  # Send validation email
		user.set_password(datas['password'])
		user.save()


		return Response({'detail': 'POST answer', 'message': 'success'})

	def sendEmail(self, datas):
		c = Context({'password': datas['password'], 'username': datas['email']})
		f = open(settings.STATIC_PATH + datas['email_path'], 'r')
		t = Template(f.read())
		f.close()
		message = t.render(c)
		# print unicode(message).encode('utf8')
		send_mail(datas['email_subject'], message, 'zaafoonoreply@gmail.com', [datas['email']], fail_silently=False)
		email = EmailMessage(datas['email_subject'], 'message', [datas['email']])
		email.send()

class RegistrationView(APIView):
	"""
	"""

	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		if "user" not in data or "password" not in data or "email" not in data:
			return Response(
				'parameters mising',
				status=status.HTTP_401_UNAUTHORIZED
			)
		if User.objects.filter(username=request.data['user']).exists():
			return Response(
				'Username exists',
				status=status.HTTP_400_BAD_REQUEST
			)
		if User.objects.filter(email=request.data['email']).exists():
			return Response(
				'Email exists',
				status=status.HTTP_400_BAD_REQUEST
			)
		datas = {}
		first_name = ""
		last_name = ""

		datas['username'] = request.data['user']
		datas['email'] = request.data['email']
		datas['password1'] = request.data['password']
		try:
			datas['first_name'] = request.data['first_name']
			datas['last_name'] = request.data['last_name']
		except:
			pass
		form=RegistrationForm()
		form.fields["username"]=datas['username']
		form.fields["email"] = datas['email']
		form.fields["password1"] = datas['password1']
		form.fields["password2"] = datas['password1']
		# We will generate a random activation key
		salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
		usernamesalt = datas['username']
		if isinstance(usernamesalt, unicode):
			usernamesalt = usernamesalt.encode('utf8')
		datas['activation_key'] = hashlib.sha1(salt + usernamesalt).hexdigest()

		datas['email_path'] = "/ActivationEmail.txt"
		datas['email_subject'] = "[zaafoo] Please Confirm Your E-mail Address"

		form.sendEmail(datas)  # Send validation email
		form.save(datas)  # Save the user and his profile

		username = request.data['user']
		password = request.data['password']
		print(username)
		""" Exception block req """
		user = authenticate(username=username, password=password)
		if user is None:
			user = authenticate(username=datas['email'], password=password)
		if user is None:
			return Response(
				'No default user, please create one',
				status=status.HTTP_404_NOT_FOUND
			)
		token = Token.objects.get_or_create(user=user)
		return Response({'detail': 'POST answer', 'token': token[0].key})

class Localities(APIView):
	def post(self,request,format=None):
		try:
			data=request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		city=request.data['cityid']
		locs=[]
		localities=Locality.objects.filter(city=city)

		for loca in localities:
			locs.append({'id':loca.id,'name':loca.locality_name})
		return Response({'detail': 'POST answer', 'locailities': locs})

class CityRestaurants(APIView):
	def post(self,request,format=None):
		try:
			data=request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		city=request.data['cityid']
		localities=Locality.objects.filter(city=city)
		rests=Restaurant.objects.filter(locality__in=localities).select_related()

		allloc=[]

		for locs in localities:
			rests = Restaurant.objects.filter(locality=locs).select_related()

			rest_list = []
			for rest in rests:
				resdis=RestaurantDiscount.objects.get(restaurant=rest)
				restimages = Restaurant_Images.objects.filter(restaurant=rest, type_name=0).first()
				rank = CustomerFeedback.objects.filter(restaurant=rest).aggregate(Avg('rating'))['rating__avg']
				rest_list.append({"id":rest.id,"rname":rest.rname,"street_address":rest.street_address,
								  "description":rest.description,"imagecontent": str(restimages.imagecontent),'rating':rank,'discount_pc':resdis.discountpc})
			allloc.append({'locality_name':locs.locality_name,'id':locs.id,'rest':rest_list})



		return Response({'detail': 'POST answer', 'locationaandrest': allloc})

class RestaurantMenuandTable(APIView):
	def post(self,request,format=None):
		try:
			data=request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		restid=request.data['restid']

		resto=Restaurant.objects.filter(id=restid).first()
		resim=Restaurant_Images.objects.filter(restaurant__id=restid).first()
		rstd=[{'name':resto.rname,'street_address':resto.street_address,'description':resto.description,
			   'lat':resto.lat,'lon':resto.lon,'im_url':resim.imagecontent.url}]
		print(resto)
		customerfeedback = CustomerFeedback.objects.filter(restaurant=resto).select_related()
		feedback_list=[]
		for feedback1 in customerfeedback:
			feedback_list.append({"id":feedback1.id,"restaurant":feedback1.restaurant_id,
								  "User":feedback1.customer.username,"title":feedback1.title,
								  "description":feedback1.description,"rating":feedback1.rating })
		rank = CustomerFeedback.objects.filter(restaurant=resto).aggregate(Avg('rating'))['rating__avg']
		menus = Menu.objects.filter(restaurant=resto).select_related('cuisine')
		menu_list=[]
		for menu in menus:
			menu_list.append({"id":menu.id,"name":menu.name,"description":menu.description,
							  "food_timings":menu.food_timings.name,"restaurant":menu.restaurant.rname,
							  "Veg":menu.Veg,"cuisine":menu.cuisine.type_name,"price":menu.price})
		floor = Floor.objects.filter(restaurant=resto).first()
		print(floor)
		flp = FloorPlan.objects.filter(floor=floor)
		print(flp)
		flp_list=[]
		for flp1 in flp:
			flp_list.append({"id":flp1.id,"floorid":flp1.floor.id,
							 "point":flp1.point,"next_point":flp1.next_point,"x":flp1.x,"y":flp1.y})
		tabls = Tables.objects.filter(floor=floor)
		print(tabls)
		tables_list=[]
		for table in tabls:
			tables_list.append({"id":table.id,"floorid":table.floor.id,
								"personstosit":table.personstosit,"next_point":table.next_point,
								"x":table.x,"y":table.y})
		num1 = int(tabls[0].personstosit)
		packs = []
		lims = 1
		resdis=None
		resdis2=[]
		try:
			resdis=RestaurantDiscount.objects.get(restaurant=resto)
			resdis2.append({'Discount_Price':resdis.minprice,'pc':resdis.discountpc,'tablebookingwaivermin':resdis.tablebookingwaivermin})
		except:
			resdis2.append({'Discount_Price': 0, 'pc': 0,
							'tablebookingwaivermin': 0})

		for y in range(1, tabls.count() + 1):
			packs.append([str(lims) + " to " + str(lims + 4 - 1), y])
			lims = lims + 4

		print(packs)
		flps = ""
		for flpp in flp:
			flps = flps + str(flpp.x * 100) + ',' + str(flpp.y * 100) + ' '
		floortext = FloorTexts.objects.filter(floor=floor)
		flrtxt=[]
		for f in floortext:
			flrtxt.append({'x':f.x,'y':f.y,'degree':f.degree,'txt':f.txt})
		return Response({'detail': 'POST answer', 'menus': menu_list,
						 'flp':  flps[:-1],'tabls':tables_list, 'packs': packs,
						 'feedbacks':feedback_list,"rank":rank,"floortext":flrtxt,'res_details':rstd,'Discount':resdis2})

class LocalRestaurants(APIView):
	def post(self,request,format=None):
		try:
			data=request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		locs=request.data['localityid']

		rests=Restaurant.objects.filter(locality=locs)
		rest_list=[]
		for rest in rests:
			rest_list.append({"id":rest.id,"rname":rest.rname,
							  "street_address":rest.street_address,"description":rest.description,"locality":rest.locality.locality_name})
		restimages=Restaurant_Images.objects.filter(restaurant__in=rests,type_name=0)
		restimages_list=[]
		for restim in restimages:
			print(restim.imagecontent)
			restimages_list.append({"restid":restim.restaurant_id,"imagecontent":str(restim.imagecontent)})
		print(restimages)
		feedb=[]
		for r in rests:
			customerfeedback = CustomerFeedback.objects.filter(restaurant=r).select_related()
			rank = CustomerFeedback.objects.filter(restaurant=r).aggregate(Avg('rating'))['rating__avg']
			feedb.append({'restid':r.id,'rating':rank})

		return Response({'detail': 'POST answer', 'restimages': restimages_list,'ratings':  feedb,'rests':rest_list})

class CusResBook(APIView):
		def post(self,request, format=None):
			try:
				data = request.data
			except ParseError as error:
				return Response(
					'Invalid JSON - {0}'.format(error.detail),
					status=status.HTTP_400_BAD_REQUEST
				)
			restid = request.data['restid']
			resto=[]
			rest = Restaurant.objects.filter(id=restid).first()
			for restx in rest:
				resto.append({"id":restx.id,"rname":restx.rname,"street_address":restx.street_address,"locality":restx.locality,"description":restx.description})
			menus = Menu.objects.filter(restaurant=rest)
			floor = Floor.objects.filter(restaurant=rest).first()
			flp = FloorPlan.objects.filter(floor=floor)
			tabls = Tables.objects.filter(floor=floor)
			floortext = FloorTexts.objects.filter(floor=floor)
			flps = ""
			for flpp in flp:
				flps = flps + str(flpp.x * 100) + ',' + str(flpp.y * 100) + ' '
			return Response( {'resmenu': serializers.serialize("json",menus), 'flp': flps[:-1],
							  'tabls': serializers.serialize("json",tabls),
							  'floortext': serializers.serialize("json", floortext)})

class   AuthView(APIView):
	"""
	Authentication is needed for this methods
	"""
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,) 
	def post(self, request, format=None):
		user=request.user
		return Response({'username':user.username,'name':user.first_name+' '+user.last_name,'email':user.email,'profile_pic':' '})

class ChangePassword(APIView):
	"""
	Authentication is needed for this methods
	"""
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def post(self, request, format=None):
		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		if "oldpassword" not in data or "newpassword" not in data:
			return Response(
				'parameters not provided',
				status=status.HTTP_404_NOT_FOUND
			)

		user = request.user
		userx = authenticate(username=user.username, password=request.data['oldpassword'])
		if userx is None:
			return Response(
				'Old password doesnot match',
				status=status.HTTP_404_NOT_FOUND
			)
		if request.data['newpassword'] is request.data['oldpassword']:
			return Response(
				'Old password cannot be same as new password',
				status=status.HTTP_404_NOT_FOUND
			)

		user.set_password(request.data['newpassword'])
		user.save()

		return Response({'status':200,'message':'Password was changed successfully'})

class FacebookLoginOrSignup(APIView):

	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request):

		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)

		# try:
		access_token= request.data['access_token']
		app = SocialApp.objects.get(provider="facebook")
		token = SocialToken(app=app, token=access_token)
		print (token)
		#if token is None:
		# check token against facebook
		login = fb_complete_login(self.request._request,app, token)
		login.token = token
		login.state = SocialLogin.state_from_request(self.request._request)

		# add or update the user into users table
		ret = complete_social_login(request, login)
		userx=SocialToken.objects.filter(token=token).first().account.user
		token2 = Token.objects.get_or_create(user=userx)
		mobile = "null"
		userext = None
		try:
			userext = UserExtra.objects.get(user=userx)
			if userext.isMobileVerified:
				mobile = userext.mobile
		except Exception as e:
			print(e)
		# if we get here we've succeeded
		return Response(status=200, data={
				'success': True,
				'username': request.user.username,
				'user_id': request.user.pk,
				'token':token2[0].key,'mobile':mobile
			})
        #
		# except Exception, e:
        #
		# 	return Response(status=401, data={
		# 		'success': False,
		# 		'reason': str(e),
		# 	})

class ConfirmTransaction(APIView):
#
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})


	def post(self, request, format=None):
		print(request.data)
		bidstr=""
		datadump=""
		bsuccess=0
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
			 'Invalid JSON - {0}'.format(error.detail),
			 status=status.HTTP_400_BAD_REQUEST
			 )
		try:
			bidstr=data["bookingid"]
			datadump=data["transactiondump"]
			bsuccess=int(data["issuccess"])
		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		try:
			bookingid=int(float(bidstr))
		except:
			return Response(
				'Invalid Bookingid',
				status=status.HTTP_400_BAD_REQUEST
			)
		bok = Booking.objects.get(id=bookingid)
		if bsuccess==1:
			bok.booking_Success=True
		else:
			bok.booking_Success = False
		bok.save()
		tran = Transaction(booking=bok, transactiondump=datadump)
		tran.save()
		userext = UserExtra.objects.get(user=request.user)
		r = requests.get(
			'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=6xN8q3BrxEKD5VKj21ISvw&senderid=ZAAFOO&channel=2&DCS=0&flashsms=0&number=91' +
			userext.mobile + '&text=Your  bill ' + str(
				bok.total) + ' has been paid for ' + bok.restaurant.rname + '&route=1')
		tb = TableBooking.objects.filter(booking=bok).first()
		# itz=pytz.timezone('Asia/Calcutta')
		#
		print(tb.start.tzinfo)
		# if(itz<>tb.start.tzinfo):
		#     print('sddfsdfs')
		tmd = tb.start - timedelta(minutes=30)
		ap = ""
		if tmd.hour < 12:
			ap = 'AM'
		else:
			ap = 'PM'
		ts = str(tmd.year) + '/' + str(tmd.month) + '/' + str(tmd.day) + ' ' + str(tmd.hour) + ':' + str(
			tmd.minute) + ':' + str(tmd.second) + '0 ' + ap
		print(ts)
		r1 = requests.get(
			'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=6xN8q3BrxEKD5VKj21ISvw&senderid=ZAAFOO&channel=2&DCS=0&flashsms=0&number=91' +
			userext.mobile + '&text=Your Booking starts in 30 minutes&schedtime=' + str(tmd.year) + '/' + str(
				tmd.month) + '/' + str(tmd.day) + ' ' + str(tmd.hour) +
			':' + str(tmd.minute) + ':' + str(tmd.second) + '0 ' + ap + '&route=1')
		return Response({'detail': 'POST answer', 'Message': 'Success'})

class VerifyMobile(APIView):
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		user=request.user
		mob=''
		otp=''
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
			 'Invalid JSON - {0}'.format(error.detail),
			 status=status.HTTP_400_BAD_REQUEST
			 )
		try:
			mob=data["mob"]
			otp=data["otp"]
		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		userext=None
		try:
			userext = UserExtra.objects.get(user=user)
		except Exception as e:
			print(e)
		if userext is None:
			userext=UserExtra(user=user,mobile=mob,otp=otp,isMobileVerified=True)
			userext.save()
		else:
			userext.mobile=mob
			userext.otp=otp
			userext.isMobileVerified=True
			userext.save()
		user.is_active = True;
		user.save()
		return Response({'detail': 'POST answer', 'Message': 'Success'})

class GetUSerEmail(APIView):
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		user=request.user
		mobile="null"
		userext=None
		try:
			userext = UserExtra.objects.get(user=user)
			if userext.isMobileVerified:
				mobile = userext.mobile
		except Exception as e:
			print(e)

		email=""
		try:
			email=user.email
		except:
			email="NA"
		return Response({'detail': 'POST answer', 'email': email,'mobile':mobile,'username':user.username,'name':user.first_name+' '+user.last_name,})

class MyTransactions(APIView):
#
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})


	def post(self, request, format=None):
		user=request.user
		booking=Booking.objects.filter(customer=user)
		booking2=[]
		for b in booking:

			tabx=[]
			tables = TableBooking.objects.filter(booking=b)
			for t in tables:
				#itz = timezone.get_current_timezone()
				tabx.append({'id':t.table.id,'start':t.start,'end':t.end,'noofpersons':t.noofpersons,'price':t.price})
			exb=ExtraBooking.objects.filter(booking=b).first()
			bill=fetch_bill(b.id)
			booking2.append({'id':b.id,'restaurant_name':b.restaurant.rname,'restaurant':b.restaurant.id,
							 'foodtotal':b.foodtotal,'tabletotal':b.tabletotal,'advance':b.advance,'tax':b.tax,
							 'totalpaid':b.totalpaid,'total':b.total,'bookimg_success':b.booking_Success,
							 'customer_arrived':b.customer_arrived,'customer_left':b.customer_left,
							 'tables':tabx,'bill':bill,'cancelled':b.is_cancelled})
		#
		#tables=TableBooking.objects.filter(booking__in=booking)
		#status=BookingStatus.objects.filter(booking__in=booking)
		return Response({'detail': 'POST answer', 'bookings': booking2})
		#'menubookings':menus,'tablebooking':tables,'status':status


class BookRestaurant(APIView):
#{u'tables': 3, u'PersonalRequest': u'Enter Your Personal Request Here...', 'restid': u'3', u'tablnos': [{u'tableid': 9}, {u'tableid': 10}, {u'tableid': 11}], u'menus': [{u'menuid': 18, u'value': 3}, {u'menuid': 16, u'value': 2}], u'date': u'2017-01-25T16:21'}
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})


	def post(self, request, format=None):
		print(request.data)
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
			 'Invalid JSON - {0}'.format(error.detail),
			 status=status.HTTP_400_BAD_REQUEST
			 )
#		readdata=json.loads(request.data)
#		print(readdata)
		try:
			tables=data["tablnos"]
			restid=data["restid"]
			menus=data["menus"]
			dateraw=dateutil.parser.parse(data["date"])
			print(dateraw)
			pr=data["Personal Request"]

		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		basketitems = []
		noitems = 0
		totprice = 0

		datebooked = dateraw
		bid=bill_confirm(menus,tables,int(restid),dateraw,request.user.id,pr,False)

		return Response({'detail': 'POST answer','BookingID':bid})

class FetchRestrauntBookings(APIView):
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):

		try:
			data = request.data
		except ParseError as error:
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		bookingdate = request.data['datetime']
		print(bookingdate)
		restid = request.data['restid']
		resto = Restaurant.objects.filter(id=restid)
		bookings = Booking.objects.filter(restaurant=resto)
		datb1 = dateutil.parser.parse(bookingdate)

		tablbook = TableBooking.objects.filter(booking__in=bookings,
											   start__lte=(datb1 + timedelta(hours=3)),
											   start__gte=datb1)
		tablbook2 = RestaurantTableBlock.objects.filter(table__floor__restaurant__id=restid,
														start__lte=(datb1 + timedelta(hours=2)),
														start__gte=datb1- timedelta(hours=1))
		print(tablbook2)
		tbooks=[]
		temptables=[]
		for t in tablbook:
			temptables.append(t.table.id)
		for t in tablbook2:
			if t.table.id not in temptables:
				temptables.append(t.table.id)

		for t in temptables:
			tbooks.append({'id':t})
		return Response({'detail': 'POST answer', 'bookedtables': tbooks})


class GiveFeedback(APIView):
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)

	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		restid=0
		desc=""
		title=""
		rating=0
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		try:
			restid = int(data["restid"])
			desc = data["description"]
			title = data["title"]
			rating=int(data["rating"])
		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		rest = Restaurant.objects.get(id=restid)
		try:
			CustomerFeedback.objects.filter(customer=request.user,restaurant=rest).delete()
		except:
			print("eh")
		feedback=CustomerFeedback(customer=request.user,restaurant=rest,title=title
                                      ,description=desc,rating=rating)
		feedback.save()
		return Response({'detail': 'POST answer', 'Message': 'Success'})

class GetFeedback(APIView):
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})

	def post(self, request, format=None):
		restid = 0
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		try:
			restid = int(data["restid"])
		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		rest = Restaurant.objects.get(id=restid)
		fs=CustomerFeedback.objects.filter(restaurant=rest)
		fds=[]
		for f in fs:
			fdc=CustomerFeedback.objects.filter(customer=f.customer)
			fds.append({'usename':f.customer.username,'title':f.title,'description':f.description,'rating':f.rating,'count':len(fdc)})
		return Response({'detail': 'POST answer', 'Feedbacks': fds})

class CalculateBill(APIView):
#{u'tables': 3, u'PersonalRequest': u'Enter Your Personal Request Here...', 'restid': u'3', u'tablnos': [{u'tableid': 9}, {u'tableid': 10}, {u'tableid': 11}], u'menus': [{u'menuid': 18, u'value': 3}, {u'menuid': 16, u'value': 2}], u'date': u'2017-01-25T16:21'}
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self, request, format=None):
		return Response({'detail': "GET Response"})


	def post(self, request, format=None):
		print(request.data)
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
			 'Invalid JSON - {0}'.format(error.detail),
			 status=status.HTTP_400_BAD_REQUEST
			 )
#		readdata=json.loads(request.data)
#		print(readdata)
		try:
			tables=data["tables"]
			restid=data["restid"]

			menus=data["menus"]


		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		basketitems = []
		noitems = 0
		totprice = 0
		bill=bill_calc_init(menus,tables,int(restid),False,request.user.id)

		return Response({'detail': 'POST answer','Bill':bill})

class CancelBooking(APIView):
	authentication_classes = (TokenAuthentication,)
	permission_classes = (IsAuthenticated,)
	def get(self,request,format=None):
		return Response({'detail': "GET Response",'param':"bid"})
	def post(self,request,format=None):
		try:
			data = request.data
		except ParseError as error:
			print(ParseError)
			return Response(
				'Invalid JSON - {0}'.format(error.detail),
				status=status.HTTP_400_BAD_REQUEST
			)
		try:
			bid=int(data["bid"])
		except:
			return Response(
				'Params missing',
				status=status.HTTP_400_BAD_REQUEST
			)
		booking = Booking.objects.get(id=bid)
		if TableBooking.objects.filter(booking=booking, end__gt=timezone.now() + timedelta(minutes=360)).count() > 0:
			booking.is_cancelled = True
			TableBooking.objects.filter(booking=booking).update(owner_noticed=False)
			Cancellation(booking=booking, initiated=timezone.now() + timedelta(minutes=330)).save()
			booking.save()
			return Response({'detail': 'POST answer', 'Status': "success"})
		else:
			return Response({'detail': 'POST answer', 'Status': "failed"})
