from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import *
from rest_framework.authentication import SessionAuthentication

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cities
        fields = ('city_name','id')
class LocalitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Locality
        fields = ('locality_name','id')




class EverybodyCanAuthentication(SessionAuthentication):
    def authenticate(self, request):
        return None
