from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login,logout
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404
import hashlib,random
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from  .serializers import *
import json
import dateutil.parser
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
class CityViewSet(viewsets.ModelViewSet):    
    queryset = Cities.objects.all()
    serializer_class = CitySerializer
        
# Create your views here.
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
def home(request):
    """Home page."""
    restsow=Restaurant.objects.all()
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()

    return render(request, "zaafoomob/home.html",{'restsowned':restimages})

def restaurantlogin(request):
    
    if request.method == 'POST':
        form2=LoginForm(request.POST)
        print(request.POST)
        print(form2)
         
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request,user)
            print(username)
            resowner=Restaurant_Owner.objects.get(restaurant_user=user)
            if resowner is not None:
                return HttpResponseRedirect('/dashboard')
    form1=LoginForm()
    return render(request,"zaafoomob/restaurantlogin.html",{'form': form1})
def cuslogin(request):
    errmsg=""
    next="/m/"
    if request.GET:
        next = request.GET['next']
        print(next)
        request.session['next']=next
    if request.method == 'POST':
        form2=LoginForm(request.POST)
        print(request.POST)
        print(form2)
        next=request.session.get('next', {})
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request,user)
            
            if next<>   "":
                return HttpResponseRedirect(next)
            return HttpResponseRedirect("/m/")
        else:
            errmsg="Error in username and password combination"
    form1=LoginForm()
    return render(request,"zaafoomob/cuslogin.html",{'form': form1,'errmsg':errmsg})
def reslist(request):
    user=request.user
     
    restsow=Restaurant.objects.all()
    
    print(restsow)
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()
    print(restimages)
    return render(request,'zaafoomob/reslist.html',{'restsowned':restimages})

@login_required
def dashboard(request):
    user=request.user
    resowner=Restaurant_Owner.objects.get(restaurant_user=user)

    restsow=Restaurant.objects.filter(restaurant_owner=resowner)
    
    print(restsow)
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()
    print(restimages)
    return render(request,'zaafoomob/dashboard.html',{'restsowned':restimages})
@login_required
def ownerMenuList(request,restid):
    resto=Restaurant.objects.filter(id=restid)
    menus=Menu.objects.filter(restaurant=resto)
    return render(request,'zaafoomob/ownermenulist.html',{'resmenu':menus})
@login_required    
def mycustomers(request,restid):
	bookings=Booking.objects.filter(restaurant=restid)
	culis=[]
	for x in bookings:
		cus=User.objects.filter(id=x.customer.id).first()
		if cus not in culis:
			print(cus)
			culis.append(cus)
	print(culis)
	for x in culis:
		print(x.username)
	return render(request,'zaafoomob/mycustomers.html',{'cuss':culis})
def cusResBook(request,restid):
    if request.method=='POST':
        form2=BasketForm(request.POST)
        print(request.POST)
#        print(form2)
        basketitems = json.loads(request.POST["rideschoices"])
        rest1 = request.session.get('rest', {})
        basketitems['restid']=rest1
        print(basketitems)
        
        request.session['cart'] = basketitems
        request.session['rest'] = ""
        return HttpResponseRedirect('/basket')
    request.session['rest'] = restid
    resto=Restaurant.objects.filter(id=restid)
    menus=Menu.objects.filter(restaurant=resto)
    floor=Floor.objects.filter(restaurant=resto).first()
    flp=FloorPlan.objects.filter(floor=floor)
    tabls=Tables.objects.filter(floor=floor)
    flps=""
    for flpp in flp:
        flps=flps+str(flpp.x*100)+','+str(flpp.y*100)+' '
    form1=BasketForm()
    return render(request,'zaafoomob/cusresbook.html',{'resmenu':menus,'flp':flps[:-1],'tabls':tabls,"form1":form1})
@login_required    
def basket(request):
    cart = request.session.get('cart', {})["menus"]
    basketitems=[]
    noitems=0
    totprice=0
    for x in cart:
        menukeval=[]
        menuitem=Menu.objects.filter(id=x["menuid"]).all()
        menukeval.append(menuitem)
        print(menuitem)
        print(x["value"])
        menukeval.append(x["value"])
        basketitems.append(menukeval)
        noitems=noitems+x["value"]
        totprice=totprice+ x["value"]*menuitem[0].price
    print(basketitems)
    tots=[noitems,totprice]
    if request.method=='POST':
        datebooked=request.session.get('cart', {})["date"]

        resto=Restaurant.objects.filter(id=int(request.session.get('cart', {})["restid"])).first()
        datb1=dateutil.parser.parse(datebooked)
        user=request.user
        b1 = Booking(customer=user,restaurant=resto,foodtotal=tots[1],tabletotal=0,advance=tots[1]*0.15,tax=0,totalpaid=0,total=tots[1])
        b1.save()
        for x in cart:
            menuitem=Menu.objects.filter(id=x["menuid"]).first()
            f1=MenuBooking(booking=b1,menu=menuitem,start=datb1,end=datb1+
                timedelta(hours=2),actual_price=menuitem.price,discount=0,discount_price=menuitem.price,
                qos=x["value"],total=x["value"]*menuitem.price)
            f1.save()
            print(f1)
        request.session['cart'] = ""   
        return HttpResponseRedirect('/')
    return render(request,'zaafoomob/basket.html',{"cart":basketitems,"tots":tots})


@login_required
def createMenu(request):
    if request.method=='POST':
        form2=MenuForm(request.POST)
        print(request.POST)
        print(form2)
        if form2.is_valid():
            form2.save()
            return HttpResponseRedirect('/dashboard') #add menulist redirect
    form2=MenuForm()
    user=request.user
    print(user)
    resowner=Restaurant_Owner.objects.get(restaurant_user=user)

    restsow=Restaurant.objects.filter(restaurant_owner=resowner)
    form2.fields['restaurant'].queryset=restsow
    return render(request,"zaafoomob/createmenu.html",{'form': form2})

@login_required
def deletemenu(request,menuid):
    menu=get_object_or_404(Menu,pk=menuid)
    menu.delete()
    return HttpResponseRedirect('/dashboard') #add menulist redirect
def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(home)
    registration_form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            datas={}
            datas['username']=form.cleaned_data['username']
            datas['email']=form.cleaned_data['email']
            datas['password1']=form.cleaned_data['password1']

            #We will generate a random activation key
            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
            usernamesalt = datas['username']
            if isinstance(usernamesalt, unicode):
                usernamesalt = usernamesalt.encode('utf8')
            datas['activation_key']= hashlib.sha1(salt+usernamesalt).hexdigest()

            datas['email_path']="/ActivationEmail.txt"
            datas['email_subject']="Activation de votre compte yourdomain"

            form.sendEmail(datas) #Send validation email
            form.save(datas) #Save the user and his profile

            request.session['registered']=True #For display purposes
            return HttpResponseRedirect('/')
        else:
            registration_form = form #Display form with error messages (incorrect fields, etc)

    return render(request, 'zaafoomob/register.html', {'form': registration_form})
#View called from activation email. Activate user if link didn't expire (48h default), or offer to
#send a second link if the first expired.
def activation(request, key):
    activation_expired = False
    already_active = False
    messk=''
    profil = get_object_or_404(Profil, activation_key=key)
    if profil.user.is_active == False:
        if timezone.now() > profil.key_expires:
            activation_expired = True #Display : offer to user to have another activation link (a link in template sending to the view new_activation_link)
            id_user = profil.user.id
            messk='Expired'
        else: #Activation successful
            profil.user.is_active = True
            profil.user.save()
            messk='Success'
    #If user is already active, simply display error message
    else:
        already_active = True #Display : error message
        messk='already_active'
    return render(request, 'zaafoomob/activation.html', {'msg':messk})

def new_activation_link(request, user_id):
    form = RegistrationForm()
    datas={}
    user = User.objects.get(id=user_id)
    if user is not None and not user.is_active:
        datas['username']=user.username
        datas['email']=user.email
        datas['email_path']="/ResendEmail.txt"
        datas['email_subject']="Nouveau lien d'activation yourdomain"

        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        usernamesalt = datas['username']
        if isinstance(usernamesalt, unicode):
            usernamesalt = usernamesalt.encode('utf8')
        datas['activation_key']= hashlib.sha1(salt+usernamesalt).hexdigest()

        profil = Profil.objects.get(user=user)
        profil.activation_key = datas['activation_key']
        profil.key_expires = datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
        profil.save()

        form.sendEmail(datas)
        request.session['new_link']=True #Display : new link send

    return HttpResponseRedirect('/')