from .models import *
from django.contrib.auth.models import User, Group
from datetime import datetime, timedelta
from django.utils import timezone
import math
def bill_calc_init(menu,tables,restid,fromweb,uid):
	if len(tables)==0:
		return None
	servicecharges=RestaurantServiceCharge.objects.filter(restaurant__id=restid)
	resdis = RestaurantDiscount.objects.get(restaurant__id=restid)
	taxes=RestaurantTax.objects.filter(restaurant__id=restid)
	service_taxesgen = RestaurantServiceTax.objects.filter(restaurant__id=restid,name="net").first()
	service_taxesfood = RestaurantServiceTax.objects.filter(restaurant__id=restid, name="food").first()
	menutotal=0
	ret=[]
	for x in menu:
		menuitem = Menu.objects.get(id=x["menuid"])
		ret.append({'name':menuitem.name,'qos':x["value"],'price':menuitem.price,'total':x["value"]*menuitem.price})
		menutotal= menutotal+ x["value"] * menuitem.price
	tabletotal=0
	waivercount=len(tables)-int(menutotal/resdis.tablebookingwaivermin)
	if waivercount <0:
		waivercount=0

	print(len(tables))
	ret.append(	{'name': 'Table', 'qos': len(tables), 'price': 40,
			 'total': 40*waivercount})
	tabletotal=waivercount*40
	if fromweb:
		if menutotal>=resdis.minprice:
			menutotal=menutotal-menutotal*resdis.discountpc
			ret.append(
				{'name': 'Discount', 'qos': 1, 'price': str(resdis.discountpc*100)+'%', 'total': -menutotal*resdis.discountpc})
		else:
			ret.append(
				{'name': 'Discount', 'qos': 0, 'price': '0%',
			 		'total': 0})
	else:
		user=User.objects.get(id=uid)
		cusemails = PromoEmailList.objects.filter(user=user).count()
		cuspho = PromoPhnoList.objects.filter(user=user).count()
		bookings = Booking.objects.filter(customer=user).count()
		rem = cusemails + cuspho - bookings
		if rem>0:
			if menutotal >= resdis.minprice:
				menutotal = menutotal - menutotal * resdis.discountpc
				ret.append(
					{'name': 'Discount', 'qos': 1, 'price': str(resdis.discountpc * 100) + '%',
					 'total': -menutotal * resdis.discountpc})
			else:
				ret.append(
					{'name': 'Discount', 'qos': 0, 'price': '0%',
					 'total': 0})
		else:
			ret.append(
				{'name': 'Discount', 'qos': 0, 'price': '0%',
				 'total': 0})

	servicecharge=0
	for s in servicecharges:
		ret.append(
			{'name': s.name, 'qos': 1, 'price': str(s.pc*100)+'%',
			 'total': menutotal*s.pc})
		servicecharge=servicecharge+menutotal*s.pc
	#menuservicetax=service_taxesfood.pc*menutotal
	netservicetax=service_taxesgen.pc*(servicecharge+tabletotal)

	ret.append(
			{'name': 'Service Tax', 'qos': 1, 'price': str(service_taxesgen.pc*100)+'%',
			 'total': math.ceil(netservicetax)})
	taxtotal=0
	for t in taxes:
		taxtotal=taxtotal+(menutotal)*t.pc
		ret.append(
			{'name': t.name, 'qos': 1, 'price': str(t.pc * 100) + '%',
			 'total': math.ceil((menutotal+tabletotal+servicecharge)*t.pc)})
	total=menutotal+tabletotal+servicecharge+netservicetax+taxtotal
	trancharge = math.ceil(total * 0.02041)
	ret.append(
		{'name': 'Transaction Charge', 'qos': '1', 'price': '2.04%',
		 'total': trancharge})
	ret.append(
		{'name': 'Total', 'qos': '', 'price': '',
		 'total': math.ceil(total+trancharge)})
	return ret

def bill_confirm(menu,tables,restid,bdate,uid,prsnl,fromweb):
	bid=0
	user=User.objects.get(id=uid)
	servicecharges = RestaurantServiceCharge.objects.filter(restaurant__id=restid)
	rest=Restaurant.objects.get(id=restid)
	resdis = RestaurantDiscount.objects.get(restaurant__id=restid)
	taxes = RestaurantTax.objects.filter(restaurant__id=restid)
	service_taxesgen = RestaurantServiceTax.objects.filter(restaurant__id=restid, name="net").first()

	menutotal = 0
	for x in menu:
		menuitem = Menu.objects.get(id=x["menuid"])
		menuitem = Menu.objects.filter(id=x["menuid"]).first()
		menutotal = menutotal + x["value"] * menuitem.price
	tabletotal = 0


	waivercount = len(tables) - int(menutotal / resdis.tablebookingwaivermin)
	if waivercount < 0:
		waivercount = 0


	tabletotal = 40* waivercount
	menudiscount=0
	discount=0
	if fromweb:
		if menutotal >= resdis.minprice:
			menutotal = menutotal - menutotal * resdis.discountpc
			menudiscount=resdis.minprice
			discount = resdis.discountpc
	else:
		user = User.objects.get(id=uid)
		cusemails = PromoEmailList.objects.filter(user=user).count()
		cuspho = PromoPhnoList.objects.filter(user=user).count()
		bookings = Booking.objects.filter(customer=user).count()
		rem = cusemails + cuspho - bookings
		if rem > 0:
			if menutotal >= resdis.minprice:
				menutotal = menutotal - menutotal * resdis.discountpc
				menudiscount = resdis.minprice
				discount=resdis.discountpc
	servicecharge = 0
	for s in servicecharges:
		servicecharge = servicecharge + menutotal * s.pc

	netservicetax = service_taxesgen.pc * (servicecharge + tabletotal)

	taxtotal = 0
	for t in taxes:
		taxtotal = taxtotal + (menutotal) * t.pc
	total = menutotal + tabletotal + servicecharge + netservicetax + taxtotal
	trancharge = math.ceil(total * 0.02041)
	grandtotal=total+trancharge
	b1 = Booking(customer=user, restaurant=rest, foodtotal=menutotal, tabletotal=tabletotal,
				 advance=grandtotal, tax=servicecharge + netservicetax + taxtotal,
				 totalpaid=grandtotal, total=grandtotal, booking_Success=False, customer_arrived=False,
				 personal_request=prsnl)
	b1.save()
	for x in menu:
		menuitem = Menu.objects.get(id=x["menuid"])
		menuitem = Menu.objects.filter(id=x["menuid"]).first()
		f1 = MenuBooking(booking=b1, menu=menuitem, start=bdate, end=bdate+
																	 timedelta(hours=2), actual_price=menuitem.price,
						 discount=menuitem.price * discount,
						 discount_price=menuitem.price - menuitem.price * discount,
						 qos=x["value"], total=x["value"] * (menuitem.price - menuitem.price * discount))
		f1.save()


	for y in tables:
		tableitem = Tables.objects.filter(id=y["tableid"]).first()
		if waivercount>0: #menutotal here includes discounted price, update if policy changed
			t1 = TableBooking(booking=b1, indices="all", table=tableitem, start=bdate,
							  end=bdate +timedelta(hours=2), noofpersons=tableitem.personstosit, is_filled=True, price=40)
			waivercount=waivercount-1
		else:
			t1 = TableBooking(booking=b1, indices="all", table=tableitem, start=bdate,
							  end=bdate +timedelta(hours=2), noofpersons=tableitem.personstosit, is_filled=True, price=40)
		t1.save()
	for s in servicecharges:
		servicechargetax=BillTax(booking=b1,index=0,name=s.name,pc=s.pc,amount=menutotal*s.pc)
		servicechargetax.save()

	netservicetax=BillTax(booking=b1,index=1,name="Service Tax",pc=service_taxesgen.pc,amount=netservicetax)
	netservicetax.save()
	for t in taxes:
		ttx=BillTax(booking=b1,index=1,name=t.name,pc=t.pc,amount=(menutotal + tabletotal + servicecharge) * t.pc)
		ttx.save()
	transactiontax=BillTax(booking=b1,index=1,name='Transaction Charge',pc=.02,amount=trancharge)
	transactiontax.save()
	bid=b1.id
	return bid
def book_extra(bid,menu,bdate):

	booking=Booking.objects.get(id=bid)
	user=User.objects.get(id=booking.customer.id)
	rest = Restaurant.objects.get(id=booking.restaurant.id)
	servicecharges = RestaurantServiceCharge.objects.filter(restaurant=rest)

	resdis = RestaurantDiscount.objects.get(restaurant=rest)
	taxes = RestaurantTax.objects.filter(restaurant=rest)
	service_taxesgen = RestaurantServiceTax.objects.filter(restaurant=rest, name="net").first()

	menutotal = 0
	for x in menu:
		menuitem = Menu.objects.get(id=x["menuid"])
		menuitem = Menu.objects.filter(id=x["menuid"]).first()
		menutotal = menutotal + x["value"] * menuitem.price

	menudiscount=0
	if menutotal >= resdis.minprice:
		menutotal = menutotal - menutotal * resdis.discountpc
		menudiscount=resdis.minprice
	servicecharge = 0
	for s in servicecharges:
		servicecharge = servicecharge + menutotal * s.pc

	netservicetax = service_taxesgen.pc * (servicecharge)

	taxtotal = 0
	for t in taxes:
		taxtotal = taxtotal + (menutotal ) * t.pc
	total = menutotal + servicecharge +  netservicetax + taxtotal
	#trancharge = math.ceil(total * 0.02041)
	#grandtotal=total+trancharge
	b1 = ExtraBooking(booking=booking,tax=servicecharge  + netservicetax + taxtotal,
					  subtotal=menutotal,total=total,leavingtime=bdate)
	b1.save()
	for x in menu:
		menuitem = Menu.objects.get(id=x["menuid"])
		f1 = ExtraMenuBooking(extrabooking=b1, menu=menuitem,
						 discount=menuitem.price * resdis.discountpc,actual_price=menuitem.price,
						 discount_price=menuitem.price - menuitem.price * resdis.discountpc,
						 qos=x["value"], total=x["value"] * (menuitem.price - menuitem.price * resdis.discountpc))
		f1.save()

	for s in servicecharges:
		servicechargetax=ExtraBillTax(extrabooking=b1,index=0,name=s.name,pc=s.pc,amount=menutotal*s.pc)
		servicechargetax.save()

	netservicetax=ExtraBillTax(extrabooking=b1,index=1,name="Service Tax",pc=service_taxesgen.pc,amount=netservicetax)
	netservicetax.save()

	for t in taxes:
		ttx=ExtraBillTax(extrabooking=b1,index=1,name=t.name,pc=t.pc,amount=(menutotal) * t.pc)
		ttx.save()
	#transactiontax=BillTax(booking=b1,index=1,name='Transaction Charge',pc=.02,amount=trancharge)
	#transactiontax.save()
	booking.customer_left=True
	booking.save()
	return bid
def fetch_bill(bid):
	ret= []
	booking=Booking.objects.get(id=bid)
	discount=0
	starttime=''
	endtime=''
	checkouttime=''
	try:
		menubookings=MenuBooking.objects.filter(booking=booking)
		for m in menubookings:
			ret.append(	{'name': m.menu.name, 'qos': m.qos, 'price': m.actual_price,
		 					'total': m.actual_price * m.qos})
			print(discount)
			print(m.actual_price)
			print(m.qos)
			print(m.total)
			discount = discount+m.actual_price * m.qos-m.total
	except Exception as e:
		raise e
	try:
		tablebookings=TableBooking.objects.filter(booking=booking)
		tabletotal=0
		for t in tablebookings:
			tabletotal=tabletotal+t.price
			starttime=t.start
			endtime=t.end
		ret.append({'name': 'Table', 'qos': len(tablebookings)	, 'price': 40,
				'total': tabletotal})
	except Exception as e:
		raise e
	ret.append({'name': 'Discount', 'qos': '', 'price':'' ,
				'total': -discount})
	try:
		servicecharge=BillTax.objects.filter(booking=booking,index=0)
		for s in servicecharge:
			ret.append({'name': s.name, 'qos': '', 'price':str(s.pc*100)+'%' ,
				'total': s.amount})
	except Exception as e:
		raise e
	try:
		servicecharge=BillTax.objects.filter(booking=booking,index=1)
		for s in servicecharge:
			ret.append({'name': s.name, 'qos': '', 'price':str(s.pc*100)+'%' ,
				'total': s.amount})
	except Exception as e:
		raise e

	ret.append({'name': 'Total', 'qos': '', 'price': '',
				'total': booking.total})
	try:
		discount=0
		extrabooking=ExtraBooking.objects.get(booking=booking)
		extramenu=ExtraMenuBooking.objects.filter(extrabooking=extrabooking)
		checkouttime=extrabooking.leavingtime
		for ext in extramenu:
			ret.append({'name': ext.menu.name, 'qos': ext.qos, 'price': ext.actual_price,
				'total': ext.actual_price * ext.qos})
			discount = discount + ext.actual_price * ext.qos - ext.total
		ret.append({'name': 'Discount', 'qos': '', 'price': '',
					'total': -discount})
		try:
			servicecharge=ExtraBillTax.objects.filter(extrabooking=extrabooking,index=0)
			for s in servicecharge:
				ret.append({'name': s.name, 'qos': '', 'price':str(s.pc*100)+'%' ,
					'total': s.amount})
		except Exception as e:
			raise e
		try:
			servicecharge=ExtraBillTax.objects.filter(extrabooking=extrabooking,index=1)
			for s in servicecharge:
				ret.append({'name': s.name, 'qos': '', 'price':str(s.pc*100)+'%' ,
					'total': s.amount})
		except Exception as e:
			raise e
		ret.append({'name': 'Extra Total', 'qos': '', 'price': '',
				'total': extrabooking.total})
	except Exception as e:
		print (e)
	allbookingdata=[]
	#itz = timezone.get_current_timezone()
	allbookingdata.append({'user':booking.customer.first_name+' '+booking.customer.last_name,'username':booking.customer.username,'booking_success':booking.booking_Success,
						   'customer_arrived':booking.customer_arrived,'restaurant_name':booking.restaurant.rname,'restaurant_id':booking.restaurant.id,'personal_request':booking.personal_request,
						   'start_time':starttime,'end_time':endtime,'checkout_time':checkouttime,'bill':ret})
	return allbookingdata