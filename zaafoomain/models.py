from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.
class States(models.Model):
    state_name=models.CharField(max_length=50)
    def __str__(self):              # __unicode__ on Python 2
        return self.state_name

class Cities(models.Model):
    state=models.ForeignKey(States)
    city_name=models.CharField(max_length=50)
    def __str__(self):              # __unicode__ on Python 2
        return self.city_name

class Locality(models.Model):
    city=models.ForeignKey(Cities)
    locality_name=models.CharField(max_length=50)
    def __str__(self):              # __unicode__ on Python 2
        return self.locality_name

class Cuisine(models.Model):
    description=models.CharField(max_length=100)
    type_name=models.CharField(max_length=50)
    def __str__(self):              # __unicode__ on Python 2
        return self.type_name
    
class Restaurant_Owner(models.Model):
    restaurant_user=models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    def __str__(self):  # __unicode__ on Python 2
        return self.restaurant_user.username


class Restaurant(models.Model):
    restaurant_owner=models.ForeignKey(Restaurant_Owner)
    rname=models.CharField(max_length=100)
    street_address=models.CharField(max_length=200)
    locality=models.ForeignKey(Locality)
    description=models.CharField(max_length=200)
    lat=models.FloatField(default=0.0)
    lon = models.FloatField(default=0.0)
    def __str__(self):              # __unicode__ on Python 2
        return self.rname

class Restaurant_Images(models.Model):
    restaurant=models.ForeignKey(Restaurant)
    type_name=models.IntegerField()
    imagecontent=models.ImageField(upload_to='static')
    # def save(self, *args, **kwargs):
    #     try:
    #         path1 = safe_join(os.path.abspath(settings.MEDIA_ROOT)+'\images', self.imagecontent)
    #         image_file = open(path1,'rb')
    #         file_content = image_file.read()
    #         self.imagecontent=file_content
    #     except:
    #         filename = 'no_image.png'
    #         path = safe_join(os.path.abspath(settings.MEDIA_ROOT), filename)
    #         #if not os.path.exists(path):
    #         #    raise ObjectDoesNotExist
    #         no_image = open(path, 'rb')
    #         file_content = no_image.read()
    #     super(Categories, self).save(*args, **kwargs)
    

class Food_Timings(models.Model):
    name=models.CharField(max_length=100)
    description=models.CharField(max_length=100)
    def __str__(self):              # __unicode__ on Python 2
        return self.name

class RestaurantFoodTiming(models.Model):
    restaurant=models.ForeignKey(Restaurant)
    food_timings=models.ForeignKey(Food_Timings)
    from_T=models.TimeField()
    to_T=models.TimeField()
    def __str__(self):              # __unicode__ on Python 2r
        return self.restaurant.rname + " "+ self.food_timings.name

class Floor(models.Model):
    restaurant=models.ForeignKey(Restaurant)
    floorno=models.IntegerField()
    def __str__(self):              # __unicode__ on Python 2r
        return self.restaurant.rname + " "+ str(self.floorno)

class FloorPlan(models.Model):
    floor=models.ForeignKey(Floor)
    point=models.IntegerField()
    next_point=models.IntegerField()
    x=models.FloatField()
    y=models.FloatField()
    def __str__(self):              # __unicode__ on Python 2
        return self.floor.restaurant.rname+" "+self.floor.restaurant.street_address
class Tables(models.Model):
    floor=models.ForeignKey(Floor)
    personstosit=models.IntegerField()
    next_point=models.IntegerField()
    restauranttableno=models.IntegerField(default=0)
    x=models.FloatField()
    y=models.FloatField()
    def __str__(self):              # __unicode__ on Python 2
        return self.floor.restaurant.rname
class Menu(models.Model):
    name=models.CharField(max_length=100)
    description=models.CharField(max_length=200)
    food_timings=models.ForeignKey(Food_Timings)
    restaurant=models.ForeignKey(Restaurant)
    Veg=models.BooleanField(default=True)
    cuisine=models.ForeignKey(Cuisine)
    price=models.FloatField()
    def __str__(self):              # __unicode__ on Python 2
        return self.name
class Profil(models.Model):
    user = models.OneToOneField(User, related_name='profil') #1 to 1 link with Django User
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()
class Booking(models.Model):
    customer=models.ForeignKey(User)
    restaurant=models.ForeignKey(Restaurant)
    foodtotal=models.FloatField()
    tabletotal=models.FloatField()
    advance=models.FloatField()
    tax=models.FloatField()
    totalpaid=models.FloatField()
    total=models.FloatField()
    personal_request=models.CharField(max_length=1000,blank=True)
    booking_Success=models.BooleanField()
    customer_arrived= models.BooleanField()
    customer_left=models.BooleanField(default=False)
    is_cancelled=models.BooleanField(default=False)
    def __str__(self):              # __unicode__ on Python 2
        return self.restaurant.rname+" "+self.customer.username
class TableBooking(models.Model):
    booking=models.ForeignKey(Booking)
    table=models.ForeignKey(Tables)
    start=models.DateTimeField()
    end=models.DateTimeField()
    noofpersons=models.IntegerField()
    is_filled=models.BooleanField()
    indices=models.CharField(max_length=100,blank=True)
    price=models.IntegerField(default=0)
    owner_noticed=models.BooleanField(default=False)
    def __str__(self):              # __unicode__ on Python 2
        return self.table.floor.restaurant.rname
class MenuBooking(models.Model):
    booking=models.ForeignKey(Booking)
    menu=models.ForeignKey(Menu)
    start=models.DateTimeField()
    end=models.DateTimeField()
    actual_price=models.FloatField()
    discount=models.FloatField()
    discount_price=models.FloatField()
    qos=models.IntegerField()
    total=models.FloatField()
class CustomerFeedback(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    customer=models.ForeignKey(User)
    title = models.CharField(max_length=200)
    description=models.CharField(max_length=200)
    rating=models.IntegerField()
class Transaction(models.Model):
    booking = models.OneToOneField(Booking, on_delete=models.CASCADE, primary_key=True)
    transactiondump=models.CharField(max_length=1000,blank=True)
class BookingStatus(models.Model):
    booking = models.OneToOneField(Booking, on_delete=models.CASCADE, primary_key=True)
class RestaurantDiscount(models.Model):
    restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE, primary_key=True)
    minprice=models.FloatField()
    discountpc=models.FloatField()
    tablebookingwaivermin=models.FloatField()
    def __str__(self):              # __unicode__ on Python 2
        return self.restaurant.rname
class RestaurantTableBlock(models.Model):
    table=models.ForeignKey(Tables)
    start = models.DateTimeField()
    end = models.DateTimeField()
class RestaurantTax(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    name=models.CharField(max_length=50)
    pc=models.FloatField()
class RestaurantServiceTax(models.Model):
    name_choices=(('net','net'),('food','food'))
    restaurant = models.ForeignKey(Restaurant)
    name=models.CharField(max_length=4,choices=name_choices)
    pc=models.FloatField()

class ExtraBooking(models.Model):
    booking = models.OneToOneField(Booking, on_delete=models.CASCADE, primary_key=True)
    tax = models.FloatField()
    subtotal = models.FloatField()
    total = models.FloatField()
    leavingtime=models.DateTimeField()
class ExtraMenuBooking(models.Model):
    extrabooking=models.ForeignKey(ExtraBooking)
    menu=models.ForeignKey(Menu)
    qos=models.IntegerField()
    actual_price = models.FloatField()
    discount = models.FloatField()
    discount_price = models.FloatField()
    total = models.FloatField()

class FloorTexts(models.Model):
    floor = models.ForeignKey(Floor)
    txt= models.CharField(max_length=1000, blank=True)
    x=models.FloatField()
    y=models.FloatField()
    degree=models.FloatField()
    def __str__(self):  # __unicode__ on Python 2
        return self.floor.restaurant.rname+" "+self.txt
class UserExtra(models.Model):
    user = models.OneToOneField(User)
    mobile=models.CharField(max_length=10)
    otp = models.CharField(max_length=6,default='000000')
    isMobileVerified=models.BooleanField()
class BillTax(models.Model):
    booking=models.ForeignKey(Booking)
    name=models.CharField(max_length=20,default='tax')
    index=models.IntegerField()
    pc=models.FloatField()
    amount=models.FloatField()
class ExtraBillTax(models.Model):
    extrabooking=models.ForeignKey(ExtraBooking)
    name = models.CharField(max_length=20,default='tax')
    index=models.IntegerField()
    pc=models.FloatField()
    amount=models.FloatField()

class RestaurantServiceCharge(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    name = models.CharField(max_length=50)
    pc = models.FloatField()
class PromoEmailList(models.Model):
    user=models.ForeignKey(User)
    refer_name=models.CharField(max_length=100)
    refer_email=models.CharField(max_length=50)
    mail_sent=models.BooleanField(default=False)
    joined = models.BooleanField(default=False)


class PromoPhnoList(models.Model):
    user = models.ForeignKey(User)
    refer_name = models.CharField(max_length=100)
    refer_phno = models.CharField(max_length=50)
    sms_sent = models.BooleanField(default=False)
    joined = models.BooleanField(default=False)
class VariableParams(models.Model):
    param=models.CharField(max_length=50)
    cvalue=models.CharField(max_length=50,default="")
    bvalue=models.BooleanField(default=False)
    fvalue=models.FloatField(default=0.0)

class Cancellation(models.Model):
    booking=models.OneToOneField(Booking)
    initiated=models.DateTimeField(default=timezone.now)
    amount_returned=models.FloatField(default=0.0)
    refund_initiated=models.DateTimeField(blank=True,null=True)
    refund_completed = models.DateTimeField(blank=True,null=True)
    extra_data=models.CharField(max_length=500,null=True)
    