from django.shortcuts import render, render_to_response
from django.db.models import Avg
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login,logout
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from .models import *
from .forms import *
from django.shortcuts import get_object_or_404
import hashlib,random
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from  .serializers import *
import json,math,requests
import dateutil.parser
from ebs_gateway import forms
from django.views.decorators.csrf import csrf_exempt
from .tasks import *
from .zflib import *
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
class CityViewSet(viewsets.ModelViewSet):    
    queryset = Cities.objects.all()
    serializer_class = CitySerializer
        
# Create your views here.
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
@csrf_exempt
def home(request):
    """Home page."""
    userext = None
    if(request.user.is_authenticated):
        try:
            user = request.user
            userext = UserExtra.objects.get(user=user)
            if not userext.isMobileVerified:
                return HttpResponseRedirect('/mob')
        except Exception as e:
            print(e)
        if userext is None:
            return HttpResponseRedirect('/mob')

    restsow=Restaurant.objects.all()
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()

    return render(request, "zaafoomain/home.html", {'restsowned':restimages})
def tos(request):
    return render(request,"zaafoomain/tos.html")
def contactus(request):

    return render(request,"zaafoomain/contactus.html")
def disclaimer(request):
    return render(request,"zaafoomain/disclaimer.html")
@login_required
def ebill(request):
    usr=request.user
    resowner = Restaurant_Owner.objects.get(restaurant_user=usr)
    if resowner is None:
        return HttpResponseRedirect('/')
    if request.method == 'POST':
        form2 = BasketForm(request.POST)
        print(request.POST)
        #        print(form2)
        basketitems = json.loads(request.POST["rideschoices"])
        print(basketitems)
        bid=""
        excarr=0
        excfin=0
        try:
            bid=basketitems["bookingid"]
        except:
            excarr=1
        excfin=0
        try:
            bid = basketitems["bid"]
            excfin=1
        except:
            excfin=0
        if(excarr==0):
            bid1=int(bid)
            tempb=Booking.objects.get(id=bid1)
            tempb.customer_arrived=True
            tempb.save()
        if(excfin==1):
            bid1=int(bid)
            tempb=Booking.objects.get(id=bid1)
            mq=basketitems["mq"]
            tempb.customer_left=True
            bxd=book_extra(bid1,mq,datetime.now())
            tempb.save()
            user = tempb.customer
            userext = UserExtra.objects.get(user=user)
            r = requests.get(
                'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=6xN8q3BrxEKD5VKj21ISvw&senderid=ZAAFOO&channel=2&DCS=0&flashsms=0&number=91' +
                userext.mobile + '&text=Your  bill is available at http://zaafoo.com/resbill/' + str(bid)+ '&route=1')
        return HttpResponseRedirect('/ebill')
    res=Restaurant.objects.filter(restaurant_owner=resowner).first()
    bookings = Booking.objects.filter(restaurant=res,booking_Success=True,customer_left=False)
    print(bookings)
    tablebookings= TableBooking.objects.filter(booking__in=bookings,
                                              start__gte=(datetime.now()-timedelta(minutes=30)),
                                              start__lte=(datetime.now() + timedelta(hours=2)))
    print(tablebookings)
    bookings2=[]
    for x in tablebookings:
        if x.booking not in bookings2:
            bookings2.append(x.booking)
    # add check for transaction success
    bookstata=[]
    bookstatnta=[]    # Booking.objects.filter(booking__in=bookings2,customer_arrived=False)
    for y in bookings2:
        if y.customer_arrived==False:
            bookstatnta.append(y)
        else:
            bookstata.append(y)

    notarrived=[]
    for x in bookstatnta:
        tables = ""
        tableboo = TableBooking.objects.filter(booking=x)
        for y in tableboo:
            tables = tables + str(y.table.restauranttableno) + ","
        print(tables)
        notarrived.append({"BookingID":x.id,"Tables":tables,
                           "CustomerName":x.customer.first_name+" "+x.customer.last_name})


    arrived=[]
    for x in bookstata:
        menus=[]
        menboo=MenuBooking.objects.filter(booking=x)
        for y in menboo:
            menus.append({"MenuID":y.menu.id,"MenuName":y.menu.name,"Quantity":y.qos,
                          "PerPrice":y.actual_price,"Discountpc":y.discount,"DiscountedPrice":y.discount_price,
                          "Total":y.total})
        tableboo=TableBooking.objects.filter(booking=x)
        tables=[]
        for y in tableboo:
            tables.append({"TableID":y.table.id,"Price":y.price})
        booking=Booking.objects.get(id=x.id)
        arrived.append({"BookingID":x.id,
                           "CustomerName":x.customer.first_name+" "+x.customer.last_name,
                        "PersonalRequest":x.personal_request,"Menu":menus,"Tables":tables,"Booking":booking})
    form1 = BasketForm()
    print('arrived')
    print(arrived)
    AllRestaurantMenus=Menu.objects.filter(restaurant=res)
    tax=RestaurantTax.objects.filter(restaurant=res)
    servicecharge=RestaurantServiceCharge.objects.filter(restaurant=res).first()#needed to be modified
    servicetax=RestaurantServiceTax.objects.filter(restaurant=res,name='food')
    taxes=[]
    servicecharges = RestaurantServiceCharge.objects.filter(restaurant=res)
    if request.method=="POST":
        print(request.POST)
    resdis = RestaurantDiscount.objects.get(restaurant=res)
    taxes = RestaurantTax.objects.filter(restaurant=res)
    service_taxesgen = RestaurantServiceTax.objects.filter(restaurant=res, name="net")

    return render(request,"zaafoomain/Ebill.html",{'arrived':arrived,'notarrived':notarrived,'menus':AllRestaurantMenus,'restid':res.id,
                                                   "servicecharges":servicecharges,"resdis":resdis,"taxes":taxes,"service_taxesgen":service_taxesgen,
                                                   'tax':taxes,'form1':form1,'servicecharge':servicecharge})
def cancelrefund(request):
    return render(request,"zaafoomain/cancellationrefunds.html")
def pp(request):
    return render(request,"zaafoomain/pp.html")
def careers(request):
    return render(request,"zaafoomain/careers.html")
def aboutus(request):
    return render(request,"zaafoomain/Aboutus.html")
def contracts(request):
    return render(request,"zaafoomain/Contracts.html")
def resfeedbacklist(request,resid):
    resto=Restaurant.objects.get(id=resid)
    customerfeedback = CustomerFeedback.objects.filter(restaurant=resto).select_related()
    rank = CustomerFeedback.objects.filter(restaurant=resto).aggregate(Avg('rating'))['rating__avg']

    return render(request, "zaafoomain/resfeedbacklist.html", {'rank': rank,'feedbacks':customerfeedback})

def restaurantlogin(request):
    
    if request.method == 'POST':
        form2=LoginForm(request.POST)
        print(request.POST)
        print(form2)
         
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request,user)
            print(username)
            resowner=Restaurant_Owner.objects.get(restaurant_user=user)
            if resowner is not None:
                restraunts=Restaurant.objects.filter(restaurant_owner=resowner)
                if  restraunts.count() > 1 :
                    return HttpResponseRedirect('/dashboard')
                else :
                    return HttpResponseRedirect('/dashboard2')
    form1=LoginForm()
    return render(request, "zaafoomain/restaurantlogin.html", {'form': form1})
# @login_required
# def ebspayment(request):
#     form =
def cuslogin(request):
    errmsg=""
    next=""
    uid=0
    resendemail = False
    if request.GET:
        next = request.GET['next']
        print(next)
        request.session['next']=next
    if request.method == 'POST':
        form2=LoginForm(request.POST)
        print(request.POST)
        print(form2)
        next=request.session.get('next', {})
        print(len(next))
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        user2=None
        try:
            user2=User.objects.get(username=username)
        except:
            print('no user, should exit here')
        user = authenticate(username=username, password=password)
        if user is None and user2 is not None:
            user = authenticate(username=user2.email, password=password)
        if user is not None:
            if not user.is_active:
                resendemail = True
                uid = user.id
                logout(request)
                errmsg = "You account is not active. Please check your email, where the activation link has been sent. Or else click below to resend email."
                form1 = LoginForm()
                return render(request, "zaafoomain/cuslogin.html",
                              {'form': form1, 'errmsg': errmsg, 'resendemail': resendemail, 'uid': uid})
            login(request,user)
            
            if len(next)>0:
                return HttpResponseRedirect(next)
            return HttpResponseRedirect("/")
        else:
            errmsg="Error in username and password combination"
    form1=LoginForm()
    return render(request, "zaafoomain/cuslogin.html", {'form': form1, 'errmsg':errmsg})
def reslist(request):
    user=request.user
     
    restsow=Restaurant.objects.all()
    
    print(restsow)
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()
    print(restimages)
    return render(request, 'zaafoomain/reslist.html', {'restsowned':restimages})

@login_required
def dashboard(request):
    user=request.user
    resowner=Restaurant_Owner.objects.get(restaurant_user=user)
    print(resowner)
    restsow=Restaurant.objects.filter(restaurant_owner=resowner)
    
    print(restsow)
    restimages=Restaurant_Images.objects.filter(restaurant__in=restsow,type_name=0).select_related()
    print(restimages)
    return render(request, 'zaafoomain/dashboard.html', {'restsowned':restimages})
@login_required
def dashboard2(request):
    user = request.user
    resowner = Restaurant_Owner.objects.get(restaurant_user=user)
    print(resowner.restaurant_user)
    restsow = Restaurant.objects.filter(restaurant_owner=resowner)
    print(restsow.count())
    bookings=Booking.objects.filter(restaurant__restaurant_owner=resowner,is_cancelled=False,booking_Success=True)
    bookingcount=TableBooking.objects.filter(booking__in=bookings,start__gte=datetime.now(),owner_noticed=False).values('booking__id').distinct().count()
    restimages = Restaurant_Images.objects.filter(restaurant__in=restsow, type_name=0).select_related()[0]
    print('bk')
    print(bookingcount)
    print(restimages)
    bookingcancelled = Booking.objects.filter(restaurant__restaurant_owner=resowner, is_cancelled=True)
    cancelcount = TableBooking.objects.filter(booking__in=bookingcancelled,
                                               owner_noticed=False).values('booking__id').distinct().count()
    return render(request, 'zaafoomain/dashboard2.html', {'restss': restimages,'newbookings':bookingcount,'cancelcount':cancelcount})
@login_required
def ownerMenuList(request,restid):
    resto=Restaurant.objects.filter(id=restid)
    menus=Menu.objects.filter(restaurant=resto)
    return render(request, 'zaafoomain/ownermenulist.html', {'resmenu':menus})
@login_required    
def mycustomers(request,restid):
	bookings=Booking.objects.filter(restaurant=restid)
	culis=[]
	for x in bookings:
		cus=User.objects.filter(id=x.customer.id).first()
		if cus not in culis:
			print(cus)
			culis.append(cus)
	print(culis)
	for x in culis:
		print(x.username)
	return render(request, 'zaafoomain/mycustomers.html', {'cuss':culis})
@login_required
def resbill(request,booking):
    bookings = Booking.objects.get(id=booking)
    bill=fetch_bill(booking)
    print(bill)
    resowner=Restaurant_Owner.objects.filter(restaurant_user=request.user)
    if resowner is not None:
        resowner2=resowner.first()
        if bookings.restaurant.restaurant_owner==resowner2:
            TableBooking.objects.filter(booking=bookings).update(owner_noticed=True)
    return render(request, 'zaafoomain/ResBill.html', {'bill':bill})
@login_required
def respastbook(request,restid):
    bookings = Booking.objects.filter(restaurant__id=int(restid),booking_Success=True)

    menubookings=TableBooking.objects.filter(booking__in=bookings,end__lt=datetime.now()).select_related()
    bookings2=[]
    for book in menubookings:
        print(book.booking)
        if book.booking not in bookings2:
            bookings2.append(book.booking)
    print (bookings2)
    return render(request, 'zaafoomain/respastbook.html', {'book': bookings2})
@login_required
def rescurrbook(request,restid):
    bookings = Booking.objects.filter(restaurant=restid,booking_Success=True)

    tablebookings=TableBooking.objects.filter(booking__in=bookings,end__gt=datetime.now()).select_related()

    bookings2=[]
    bookings3 = []
    for book in tablebookings:
        print(book.booking)
        if book.booking not in bookings3:
            bookings3.append(book.booking)
            bookings2.append({"Booking":book.booking,"new":book.owner_noticed})
    print (bookings2)
    return render(request, 'zaafoomain/rescurrbook.html', {'book': bookings2})
@login_required
def rescanbook(request,restid):
    bookings = Booking.objects.filter(restaurant=restid,booking_Success=True,is_cancelled=True)

    tablebookings=TableBooking.objects.filter(booking__in=bookings).select_related()

    bookings2=[]
    bookings3 = []
    for book in tablebookings:
        print(book.booking)
        if book.booking not in bookings3:
            bookings3.append(book.booking)
            bookings2.append({"Booking":book.booking,"new":book.owner_noticed})
    print (bookings2)
    return render(request, 'zaafoomain/rescurrbook.html', {'book': bookings2})
def cusResBook(request,restid):
    if request.method=='POST':
        form2=BasketForm(request.POST)
        print(request.POST)
#        print(form2)
        basketitems = json.loads(request.POST["rideschoices"])
        rest1 = request.session.get('rest', {})
        basketitems['restid']=rest1
        print('basketitems')
        print(basketitems)
        
        request.session['cart'] = basketitems
        request.session['rest'] = ""
        return HttpResponseRedirect('/basket')
    request.session['rest'] = restid
    resto=Restaurant.objects.filter(id=restid)
    customerfeedback=CustomerFeedback.objects.filter(restaurant=resto).select_related()
    rank=CustomerFeedback.objects.filter(restaurant=resto).aggregate(Avg('rating'))['rating__avg']
    if rank is None:
        rank=0.0
    print(rank)
    cuisines=[]

    menus=Menu.objects.filter(restaurant=resto)
    for men in menus:
        if men.cuisine not in cuisines:
            cuisines.append(men.cuisine)
    menu2=[]
    for cu in cuisines:
        menu2.append({'Cuisine':cu,'Menus':Menu.objects.filter(restaurant=resto,cuisine=cu)})
    
    floor=Floor.objects.filter(restaurant=resto).first()
    flp=FloorPlan.objects.filter(floor=floor)
    tabls=Tables.objects.filter(floor=floor)
    bookings=Booking.objects.filter(restaurant=resto,booking_Success=True)
    tablbook1=TableBooking.objects.filter(booking__in=bookings,
                                          start__lte=(timezone.now()+timedelta(hours=2)),
                                          start__gte=(timezone.now())).select_related()
    tablbook2 = RestaurantTableBlock.objects.filter(table__floor__restaurant__id=restid,
                                                    start__lte=(timezone.now() + timedelta(hours=2)),
                                                    start__gte=(timezone.now()))
    floortxt = FloorTexts.objects.filter(floor=floor)
    tablbook=[]
    for t in tablbook1:
        tablbook.append({'id':t.table.id})
    for t in tablbook2:
        tablbook.append({'id': t.table.id})

    print(tablbook)
    num1 = int(tabls[0].personstosit)
    packs=[]
    lims=1
    for y in range(1,tabls.count()+1):
        packs.append([str(lims)+" to "+str(lims+4-1),y])
        lims=lims+4
    tb=[]
    chaircount=1;
    for t in tabls:
        chairs=[]
        for x in range(1,t.personstosit+1):
            chairs.append({"chairno":chaircount})
            chaircount=chaircount+1
        tb.append({"id":t.id,"x":t.x,"y":t.y,"chairs":chairs,"personstosit":t.personstosit})
    flps=""
    for flpp in flp:
        flps=flps+str(flpp.x*100)+','+str(flpp.y*100)+' '
    form1=BasketForm()
    restim=Restaurant_Images.objects.filter(restaurant__id=restid).first()
    print(timezone.now())
    print(datetime.now())

    # datesafety=timezone.now().strftime("%B")+" "+str(timezone.now().day)+" "+str(timezone.now().year)+" "+str(timezone.now().hour)\
    #            +":"+str(timezone.now().minute)+":"+str(timezone.now().second)+" GMT+5:30"
    datesafety = datetime.now().strftime("%B") + " " + str(datetime.now().day) + " " + str(
        datetime.now().year) + " " + str(datetime.now().hour) \
                 + ":" + str(datetime.now().minute) + ":" + str(datetime.now().second) + " GMT+5:30"
    resdis=RestaurantDiscount.objects.get(restaurant__id=restid)
    return render(request, 'zaafoomain/cusresbook.html', {'resmenu':menu2, 'flp': flps[:-1],'discount':str(resdis.discountpc*100),
                                                          'tabls':tb, "form1":form1,'minprice':resdis.minprice,
                                                          'rest':resto[0],'restim':restim,'waiverprice':resdis.tablebookingwaivermin,
                                                          "packs":packs,"feedbacks":customerfeedback,
                                                          "rank":rank,"tablbook":tablbook,"resid":restid,'floortext':floortxt,"datesafety":datesafety })

@login_required
def basket(request):
    basketitems={}
    cart={}
    tables={}
    notables={}
    discount={}
    tots={}
    discountpc={}
    tax={}
    trancharge={}
    tablediscount={}
    ret=[]
    if 1:
        cart = request.session.get('cart', {})["menus"]
        tables=request.session.get('cart', {})["tablnos"]
        notables = request.session.get('cart', {})["tables"]
        pr=request.session.get('cart', {})["PersonalRequest"]
        basketitems=[]
        noitems=0
        totprice=0
        for x in cart:
            menukeval=[]
            menuitem=Menu.objects.filter(id=x["menuid"]).all()
            menukeval.append(menuitem)
            menukeval.append(x["value"])
            basketitems.append(menukeval)
            noitems=noitems+x["value"]
            totprice=totprice+ x["value"]*menuitem[0].price
        resto = Restaurant.objects.filter(id=int(request.session.get('cart', {})["restid"])).first()
        resdis=RestaurantDiscount.objects.filter(restaurant=resto).first()
        discount=0
        tablediscount=False
        discountpc=0
        tabletototal=0
        if resdis is not None:
            if resdis.minprice <=totprice:
                discount=totprice*resdis.discountpc
                discountpc=resdis.discountpc
            if resdis.tablebookingwaivermin>totprice:
                tabletototal=int(notables) * 40
                totprice = totprice + tabletototal
            else:
                tablediscount=True
        else:
            totprice = totprice + int(notables) * 40
        totprice=totprice-discount
        tax=0.15*totprice
        totprice=totprice+tax
        trancharge=math.ceil(totprice*0.02041)
        noitems=noitems+int(notables)
        total=totprice
        totprice=totprice+trancharge
        totprice=math.ceil(totprice)
        request.session['pricetopay'] = totprice
        tots=[noitems,totprice]
        print(notables)
        print('tots')
        print(tots)
        print(basketitems)
        ret=bill_calc_init(cart,tables,resto.id,True,request.user.id)
        if request.method=='POST':
            datebooked=request.session.get('cart', {})["date"]

            resto=Restaurant.objects.filter(id=int(request.session.get('cart', {})["restid"])).first()
            datb1=dateutil.parser.parse(datebooked)
            user=request.user
            b1=bill_confirm(cart,tables,resto.id,datb1,user.id,request.session.get('cart', {})["PersonalRequest"],True)
            request.session['Booking'] = b1
            request.session['pricetopay'] = Booking.objects.get(id=b1).total
            # request.session['cart'] = ""
            return HttpResponseRedirect('/gateway')
    # except Exception as e:
    #     raise e
    print(ret)
    return render(request, 'zaafoomain/basket.html', {"cart":ret})

@login_required
def customerfeedback(request,restid):
    rest = Restaurant.objects.get(id=restid)
    if request.method=='POST':
        form2=CustomerFeedbackForm(request.POST)
        print(request.POST)
        print(form2)

        if form2.is_valid():
            print(form2)
            CustomerFeedback.objects.filter(customer=request.user,restaurant=rest).delete()
            feedback=CustomerFeedback(customer=request.user,restaurant=rest,title=form2.cleaned_data["title"]
                                      ,description=form2.cleaned_data["description"],rating=form2.cleaned_data["rating"])
            feedback.save()
            return HttpResponseRedirect('/cusResBook/'+str(restid)) #add menulist redirect


    user=request.user
    data = {'restaurant': rest, 'Customer': user}
    form2 = CustomerFeedbackForm(initial=data)

    print(user)

    return render(request, "zaafoomain/customerfeedback.html", {'form': form2,'RestData':data})



@login_required
def createMenu(request):
    if request.method=='POST':
        form2=MenuForm(request.POST)
        print(request.POST)
        print(form2)
        if form2.is_valid():
            form2.save()
            return HttpResponseRedirect('/dashboard') #add menulist redirect
    form2=MenuForm()
    user=request.user
    print(user)
    resowner=Restaurant_Owner.objects.get(restaurant_user=user)

    restsow=Restaurant.objects.filter(restaurant_owner=resowner)
    form2.fields['restaurant'].queryset=restsow
    return render(request, "zaafoomain/createmenu.html", {'form': form2})

@login_required
def deletemenu(request,menuid):
    menu=get_object_or_404(Menu,pk=menuid)
    menu.delete()
    return HttpResponseRedirect('/dashboard') #add menulist redirect
@login_required
def ownerTableBooking(request,restid):
    resto = Restaurant.objects.filter(id=restid)
    floor = Floor.objects.filter(restaurant=resto).first()
    flp = FloorPlan.objects.filter(floor=floor)
    tabls = Tables.objects.filter(floor=floor)
    bookings = Booking.objects.filter(restaurant=resto,booking_Success=True)
    if request.method == 'POST':
        form=BasketForm(request.POST)
        if form.is_valid():
            basketitems = json.loads(request.POST["rideschoices"])
            print(basketitems)
            blocktables=basketitems["tablnos"]
            tablbook = TableBooking.objects.filter(booking__in=bookings,
                                                   start__lte=(timezone.now() + timedelta(hours=2)),
                                                   start__gte=(timezone.now()- timedelta(hours=1)))

            exctables=[]
            for t in blocktables:
                for t1 in tablbook:
                    if t["tableid"]==t1.table.id:
                        exctables.append(t["tableid"])
            blocktables1=[]
            for t in blocktables:
                blocktables1.append(t["tableid"])
            actualblockedtables=[]
            for t in blocktables1:
                if t not in exctables:
                    actualblockedtables.append(t)
            for t in actualblockedtables:
                t1=Tables.objects.get(id=t)
                b1=RestaurantTableBlock(table=t1,start=timezone.now(),end=(timezone.now() + timedelta(hours=2)))
                b1.save()


    tablbook1 = TableBooking.objects.filter(booking__in=bookings,
                                           start__lte=(timezone.now() + timedelta(hours=2)),
                                               start__gte=(timezone.now()- timedelta(hours=1)))
    tablbook2=RestaurantTableBlock.objects.filter(table__floor__restaurant__id=restid,start__lte=(timezone.now() + timedelta(hours=2)),start__gte=(timezone.now()- timedelta(hours=1)))
    tablbook=[]
    for t in tablbook1:
        tablbook.append({'id':t.table.id})
    for t in tablbook2:
        tablbook.append({'id': t.table.id})

    print(tablbook)
    num1 = int(tabls[0].personstosit)
    packs = []
    lims = 1
    for y in range(1, tabls.count() + 1):
        packs.append([str(lims) + " to " + str(lims + 4 - 1), y])
        lims = lims + 4

    flps = ""
    for flpp in flp:
        flps = flps + str(flpp.x * 100) + ',' + str(flpp.y * 100) + ' '
    form1 = BasketForm()
    tb = []
    chaircount = 1;
    for t in tabls:
        chairs = []
        for x in range(1, t.personstosit + 1):
            chairs.append({"chairno": chaircount})
            chaircount = chaircount + 1
        tb.append({"id": t.id, "x": t.x, "y": t.y, "chairs": chairs, "personstosit": t.personstosit,"restauranttableno":t.restauranttableno})

    return render(request, 'zaafoomain/resbook.html', {'flp': flps[:-1],'tabls':tb, "form1":form1,
                                                          "packs":packs,"tablbook":tablbook,"resid":restid})
def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(home)
    registration_form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            datas={}
            datas['username']=form.cleaned_data['username']
            datas['email']=form.cleaned_data['email']
            datas['password1']=form.cleaned_data['password1']

            datas['first_name']= form.cleaned_data['first_name']
            datas['last_name']= form.cleaned_data['last_name']

            #We will generate a random activation key
            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
            usernamesalt = datas['username']
            if isinstance(usernamesalt, unicode):
                usernamesalt = usernamesalt.encode('utf8')
            datas['activation_key']= hashlib.sha1(salt+usernamesalt).hexdigest()

            datas['email_path']="/ActivationEmail.txt"
            datas['email_subject']="[zaafoo] Please Confirm Your E-mail Address"

            form.sendEmail(datas) #Send validation email
            form.save(datas) #Save the user and his profile

            request.session['registered']=True #For display purposes
            return HttpResponseRedirect('/')
        else:
            registration_form = form #Display form with error messages (incorrect fields, etc)

    return render(request, 'zaafoomain/register.html', {'form': registration_form})
#View called from activation email. Activate user if link didn't expire (48h default), or offer to
#send a second link if the first expired.
def activation(request, key):
    activation_expired = False
    already_active = False
    messk=''
    profil = get_object_or_404(Profil, activation_key=key)
    if profil.user.is_active == False:
        if timezone.now() > profil.key_expires:
            activation_expired = True #Display : offer to user to have another activation link (a link in template sending to the view new_activation_link)
            id_user = profil.user.id
            messk='Expired'
        else: #Activation successful
            profil.user.is_active = True
            profil.user.save()
            messk='Success'
    #If user is already active, simply display error message
    else:
        already_active = True #Display : error message
        messk='already_active'
    return render(request, 'zaafoomain/activation.html', {'msg':messk})

def new_activation_link(request, user_id):
    form = RegistrationForm()
    datas={}
    user = User.objects.get(id=user_id)
    if user is not None and not user.is_active:
        datas['username']=user.username
        datas['email']=user.email
        datas['email_path']="/ResendEmail.txt"
        datas['email_subject']="[zaafoo] Please Confirm Your E-mail Address"

        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        usernamesalt = datas['username']
        if isinstance(usernamesalt, unicode):
            usernamesalt = usernamesalt.encode('utf8')
        datas['activation_key']= hashlib.sha1(salt+usernamesalt).hexdigest()
        profil=None
        try:
            profil = Profil.objects.get(user=user)
        except:
            profil=Profil()
            profil.user=user
        profil.activation_key = datas['activation_key']
        profil.key_expires = datetime.strftime(datetime.now() + timedelta(days=4), "%Y-%m-%d %H:%M:%S")
        profil.save()

        form.sendEmail(datas)
        request.session['new_link']=True #Display : new link send

    return HttpResponseRedirect('/')
@login_required
def mob(request):
    user=request.user
    userext=None
    errmsg = ''
    try:
        userext=UserExtra.objects.get(user=user)
    except Exception as e:
        errmsg="We don't have your your phone number. Please update your phone number"
    mob=''

    verified=False
    otpsend=False
    if userext is not None:
        mob=userext.mobile
        if userext.isMobileVerified:
            verified=True
            errmsg="Verified"
    form2 = BasketForm()
    if request.method == 'POST':
        form = BasketForm(request.POST)
        if form.is_valid():
            basketitems = json.loads(request.POST["rideschoices"])
            print(basketitems)
            try:
                mob= basketitems["mob"]

                otp=random.randint(100000,999999)
                print(otp)
                #otp = basketitems["OTP"]
                r=requests.get('https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=6xN8q3BrxEKD5VKj21ISvw&senderid=ZAAFOO&channel=2&DCS=0&flashsms=0&number=91'+mob+'&text=OTP '+str(otp)+'&route=1')
                print(r.text)
                d = json.loads(r.text)
                print(d)
                print(d["MessageData"][0]["Number"])
			#
                print(d["ErrorMessage"])
                print(d["ErrorCode"])
                if d["ErrorCode"]=='000':
                    if userext is None:
                        userext=UserExtra(user=user,mobile=mob,otp=otp,isMobileVerified=False)
                        userext.save()
                    else:
                        userext.mobile=mob
                        userext.otp= otp
                        userext.isMobileVerified=False
                        userext.save()
                    errmsg="OTP is sent"
                    otpsend=True
            except Exception as e:
                print(e)
            try:
                otp = basketitems["OTP"]
                if otp==userext.otp:
                    verified=True
                    otpsend=False
                    userext.isMobileVerified=True
                    userext.save()
                    errmsg="Verified"
                else:
                    verified = False
                    otpsend = True
                    errmsg = "Otp doesnt match"
            except Exception as e:
                print(e)
    return render(request, 'zaafoomain/mob.html', {'form':form2,'verfied':verified,'otpsent':otpsend,'number':mob,'errmsg':errmsg })

def get_drugs(request,restid):
    if request.is_ajax():
        q = request.GET.get('term', '')
        drugs = Menu.objects.filter(name__icontains=q,restaurant_id=int(restid) )[:20]
        results = []
        for drug in drugs:
            drug_json = {}
            drug_json['id'] = drug.id
            drug_json['label'] = drug.name
            drug_json['value'] = drug.price
            results.append(drug_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
def test(request):
    user = request.user
    rest = Restaurant.objects.get(restaurant_owner__restaurant_user=user)
    servicecharges = RestaurantServiceCharge.objects.filter(restaurant=rest)
    if request.method=="POST":
        print(request.POST)
    resdis = RestaurantDiscount.objects.get(restaurant=rest)
    taxes = RestaurantTax.objects.filter(restaurant=rest)
    service_taxesgen = RestaurantServiceTax.objects.filter(restaurant=rest, name="net")
    form1 = BasketForm()
    return render(request, 'zaafoomain/cusresbook2.html',
                  {"servicecharges":servicecharges,"resdis":resdis,"taxes":taxes,"service_taxesgen":service_taxesgen,"form1":form1})