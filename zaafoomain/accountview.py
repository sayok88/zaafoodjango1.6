from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate,login,logout
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from .models import *
from .forms import *
from zflib import *
from django.shortcuts import get_object_or_404
import hashlib,random
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from  .serializers import *
import json
import dateutil.parser
from ebs_gateway import forms
from django.views.decorators.csrf import csrf_exempt


@login_required
def cuscurrbook(request):
    user = request.user
    bookings = Booking.objects.filter(customer=user,is_cancelled=False,booking_Success=True)
    menubookings=TableBooking.objects.filter(booking__in=bookings,end__gt=datetime.now()).select_related()
    bookings2=[]
    bookings3 = []
    for book in menubookings:
        print(book.booking)
        if book.booking not in bookings3:
            cancelable=False
            print(book.start)
            print(timezone.now()+timedelta(minutes=330))
            if (book.start-timezone.now()+timedelta(minutes=330)).total_seconds()/60>30:
                cancelable=True
            bookings2.append({"book":book.booking,"cancelable":cancelable})
            bookings3.append(book.booking)
    print (bookings2)
    return render(request, 'zaafoomain/cuscurrbook.html', {'book': bookings2})

@login_required
def cancelbooking(request,bid):
    booking=Booking.objects.get(id=bid)
    if TableBooking.objects.filter(booking=booking, end__gt=timezone.now()+timedelta(minutes=360)).count()>0:
        booking.is_cancelled=True
        TableBooking.objects.filter(booking=booking).update(owner_noticed=False)
        Cancellation(booking=booking,initiated=timezone.now()+timedelta(minutes=330)).save()
        booking.save()
    return HttpResponseRedirect('/cuscurrbookings')
@login_required
def cancelledbill(request,bid):
    booking=Booking.objects.get(id=bid)
    cancelation=Cancellation.objects.filter(booking=booking).first()
    bill = fetch_bill(booking.id)
    return render(request, 'zaafoomain/cancelledbill.html', {'bill':bill,'can':cancelation})


@login_required
def cancelations(request):
    booking=Booking.objects.filter(customer=request.user,is_cancelled=True,booking_Success=True)

    return render(request, 'zaafoomain/cuscanceledbook.html', {'book': booking})




@login_required
def MyTransactions(request):
    user = request.user

    bookings = Booking.objects.filter(customer=user)

    menubookings=TableBooking.objects.filter(booking__in=bookings,end__lt=datetime.now()).select_related()
    #add table bookings also
    bookings2=[]
    for book in menubookings:
        print(book.booking)
        if book.booking not in bookings2:
            bookings2.append(book.booking)
    print (bookings2)
    return render(request, 'zaafoomain/cuspastbook.html', {'book': bookings2})
def change_password(request):
    user = request.user
    passwrdchange_form = ChangePasswordForm()
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            u = User.objects.get(username=user.username)

            u.set_password(form.cleaned_data['password1'])
            u.save()
            return HttpResponseRedirect('/')
        else:
            passwrdchange_form=form
    return render(request, 'zaafoomain/changepassword.html', {'form': passwrdchange_form})
def profile_view(request):
    user = request.user
    form = EditProfileForm(initial={'first_name':user.first_name, 'last_name':user.last_name})
    context = {
        "form": form
    }
    return render(request, "zaafoomain/profile.html", context)

def edit_profile(request):

    user = request.user
    form = EditProfileForm(request.POST or None, initial={'first_name':user.first_name, 'last_name':user.last_name})
    if request.method == 'POST':
        if form.is_valid():


            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']

            user.save()
            return HttpResponseRedirect('/profile')

    context = {
        "form": form
    }

    return render(request, "zaafoomain/editprofile.html", context)
