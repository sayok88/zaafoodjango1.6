from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from django.db.models import Avg
from rest_framework import HTTP_HEADER_ENCODING, exceptions
from rest_framework.decorators import permission_classes
from allauth.socialaccount import providers
from allauth.socialaccount.models import SocialLogin, SocialToken, SocialApp
from allauth.socialaccount.providers.facebook.views import fb_complete_login
from allauth.socialaccount.helpers import complete_social_login
import hashlib,random
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication #,CustomTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.core import serializers
import dateutil.parser
import json
from .forms import *
from django.contrib.auth import authenticate
from .models import *
from .forms import *
from django.forms.models import model_to_dict
from .serializers import EverybodyCanAuthentication
from django.core.serializers.json import DjangoJSONEncoder
from  .zflib import  *

class PromoOn(APIView):

    def get(self, request, format=None):
        prm=False
        try:
            prm=VariableParams.objects.filter(param="promo").first().bvalue
        except:
            pass
        return Response({'detail': "GET Response",'value':prm})
class VarParamRead(APIView):
    def get(self, request, format=None):
        return Response({'detail': "GET Response",'Post_Format':"var_name=foo"})

    def post(self, request, format=None):

        try:
            data = request.data
        except ParseError as error:
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
            )
        variable=None
        ret=[]


        try:
            variable = request.data['var_name']
            prm=VariableParams.objects.filter(param=variable).first()
            ret.append({'b':prm.bvalue,'c':prm.cvalue,'f':prm.fvalue})
        except:
            return Response(
                'Params missing or incorrect param given',
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response({'detail': "GET Response", 'ret': ret})



class StoreEmailPhno(APIView):
#{u'tables': 3, u'PersonalRequest': u'Enter Your Personal Request Here...', 'restid': u'3', u'tablnos': [{u'tableid': 9}, {u'tableid': 10}, {u'tableid': 11}], u'menus': [{u'menuid': 18, u'value': 3}, {u'menuid': 16, u'value': 2}], u'date': u'2017-01-25T16:21'}
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        return Response({'detail': "GET Response"})


    def post(self, request, format=None):
        print(request.data)
        data=[]
        try:
            data = request.data
        except ParseError as error:
            print(ParseError)
            return Response(
                'Invalid JSON - {0}'.format(error.detail),
                status=status.HTTP_400_BAD_REQUEST
                )
#        readdata=json.loads(request.data)
#        print(readdata)
        emails=[]
        phno=[]
        count=0
        try:
            emails=data["emails"]
            phno=data["phnos"]
        except:
            pass
        cusemails=PromoEmailList.objects.filter(user=request.user)
        for email in emails:
            currem=None
            try:
                currem=cusemails.filter(refer_email=email["email"]).first()
            except:
                pass
            if currem is None:
                try:
                    PromoEmailList(user=request.user,refer_name=email["name"],refer_email=email["email"],mail_sent=False,joined=False).save()
                except:
                    pass
                count=count+1
        cuspho=PromoPhnoList.objects.filter(user=request.user)
        for p in phno:
            currem=None
            try:
                currem=cuspho.filter(refer_phno=p["phno"]).first()
            except:
                pass
            if currem is None:
                try:
                    PromoPhnoList(user=request.user,refer_name=p["name"],refer_phno=p["phno"],sms_sent=False,joined=False).save()
                except:
                    pass
                count = count + 1
        return Response({'detail': "Post Response","uploaded":count})

class GetPromoBookingRemaining(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        cusemails = PromoEmailList.objects.filter(user=request.user).count()
        cuspho = PromoPhnoList.objects.filter(user=request.user).count()
        bookings=Booking.objects.filter(customer=request.user).count()
        rem=cusemails+cuspho-bookings
        return Response({'detail': "GET Response","rem":rem})
