/**
 * Created by sayok bhattacharya on 2/4/2017.
 */

var Tables=[];/* {tableid:1,booked:false,selected:false,capacity:4,dom:domobject}*/

function getTable(tableid) {
    for(var i=0;i<Tables.length;i++)
    {
        if(tableid==Tables[i].tableid)
            return Tables[i];
    }
}
function bookTable(tableid) {
    table=getTable(tableid);
    table.booked=true;
    var cssClass = table.dom.getAttribute("class");
    table.dom.setAttribute("class", cssClass + " booked");

}
function selectTable(tableid) {
    table=getTable(tableid);
    table.selected=true;
    var cssClass = table.dom.getAttribute("class");
    table.dom.setAttribute("class", cssClass + " selected");

}
function addTable(tableid,booked,selected,capacity,domelem) {
    Tables.push({tableid:tableid,booked:booked,selected:selected,capacity:capacity,dom:domelem});
}
function clearTablefields() {
    for(count=0;count<Tables.length;count++) {
        cssClass = Tables[count].dom.getAttribute("class");
        if (cssClass.indexOf("selected") == -1) {      }
        else {
            Tables[count].dom.setAttribute("class", cssClass.substr(0, cssClass.indexOf("selected")));
            }
        if (cssClass.indexOf("booked") == -1) {      }
        else {
            Tables[count].dom.setAttribute("class", cssClass.substr(0, cssClass.indexOf("booked")));
       }
        Tables[count].selected=false;
        Tables[count].booked=false;

    }
}
function clearchosenTable() {
    for(count=0;count<Tables.length;count++) {
        cssClass = Tables[count].dom.getAttribute("class");
        if (cssClass.indexOf("selected") == -1) {      }
        else {
            Tables[count].dom.setAttribute("class", cssClass.substr(0, cssClass.indexOf("selected")));
            }

        Tables[count].selected=false;


    }
}
function getemptychairs() {
    empty=0;
    for(count=0;count<Tables.length;count++)
    {
        if(Tables[count].booked==false)
        {
            empty=empty+Tables[count].capacity;
        }
    }
    return empty;
}
function getchosentables() {
    tempTables=[];
    for(count=0;count<Tables.length;count++)
    {
        if(Tables[count].selected==true)
        {
            tempTables.push(Tables[count]);
        }
    }
    return tempTables;
}

function sortNumber(a,b) {
    return a - b;
}
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

// usage example:
var a = ['a', 1, 'a', 2, '1'];
var unique = a.filter( onlyUnique ); // returns ['a', 1, 2, '1']
function findUnique(arr) {
    var uniq = new Map();
    var result = new Set();
    // iterate through array
    for(var i=0; i<arr.length; i++) {
        var v = arr[i];
        // add value to map that contains counts
        if(uniq.has(v)) {
            uniq.set(v, uniq.get(v) + 1);
            // count is greater than 1 remove from set
            result.delete(v);
        } else {
            uniq.set(v, 1);
            // add a possibly uniq value to the set
            result.add(v);
        }
    }
    // set to array O(n)
    var a = [], x = 0;
    result.forEach(function(v) { a[x++] = v; });
    return a;
}

function getemptytablecapacities() {
    etc=[];
    tempmin=Tables[0].capacity;
    for(count=0;count<Tables.length;count++)
    {
        if(!Tables[count].selected||!Tables[count].booked)
        {
            etc.push(Tables[count].capacity);
        }
    }
    etc1=etc.filter(onlyUnique);
    etc1.sort(sortNumber);
    return etc1;
}
function booktablebycapacity(cap) {
    for(count=0;count<Tables.length;count++)
    {
        if(!Tables[count].selected&&!Tables[count].booked&&Tables[count].capacity==cap)
        {
            selectTable(Tables[count].tableid);
            break;
        }
    }
}
function filltable(nop) {
    clearchosenTable();
    tempnop=nop;
    if(getemptychairs()<nop)
    {
        return;
    }
    while(tempnop>0)
    {
        etc=getemptytablecapacities();
        i=0;
        for(;i<etc.length;i++)
        {
            if(tempnop<=etc[i])
            {
                break;
            }
        }
        if(i>=(etc.length-1))
        {
            booktablebycapacity(etc[etc.length-1]);
            tempnop=tempnop-etc[etc.length-1];
        }
        else
        {
            booktablebycapacity(etc[i]);
            tempnop=tempnop-etc[i];
        }
    }
}
function unselecttable(id) {
    table=getTable(id);
    cssClass = table.dom.getAttribute("class");
        if (cssClass.indexOf("selected") == -1) {      }
        else {
            table.dom.setAttribute("class", cssClass.substr(0, cssClass.indexOf("selected")));
            }
    table.selected=false;

}