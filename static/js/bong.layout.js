/*! Copyright (c) 2016 Saptarshi Bhattacharya

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
var Layout = (function(){
	// Private variables
	var $,
		$error = {
			missingIdentifier: "Missing identifier",
		},
		$warning = {
			contradiction: "Map and Data should not be provided at the same time. Data will be overwritten.",
			improperMapData: "Data provided for map is improper. Data ignored."
		}
		$parser = new DOMParser();

	var Layout = function(params){
		if(typeof params.wrapper_id !== "string") throw $error.missingIdentifier;
		if(params.map && params.data) { console.warn($warning.contradiction) }

		$ = this;
		this.wrapper_id = params.wrapper_id;
		this.data = params.data instanceof Array ? params.data : [] 
		this.map = params.map  instanceof Array ? params.map : undefined;
		this.afterRender = typeof params.afterRender == "function" ? params.afterRender: undefined;

		if(this.map) this.data = $getDataFromMap(this.map);
		$render();
		if(this.afterRender) this.afterRender();
	}
	// Layout.prototype = {
		
	// }

	//Private functions
	var $getDataFromMap = function(map){
		var data = [],
			data_object;

		for(var i = 0, len = map.length; i < len; i++){
			if(!$verifyMapElementFormat(map[i])){
				continue;
			}

			var l = (map[i].transforms && map[i].transforms.length)
					|| (map[i].fills && map[i].fills.length)
					|| (map[i].classes && map[i].classes.length)
					|| (map[i].events && map[i].events.length)
					|| (map[i].dom_attributes && map[i].dom_attributes.length)
					|| 0;

			if(l == 0) {
				data.push({
					model: map[i].model,
				});
			}

			for(var j = 0; j < l; j++){
				data_object = {};
				data_object.model = map[i].model;
				data_object.transform = map[i].transform ? JSON.parse(JSON.stringify(map[i].transform)): {};
				data_object.events = map[i].event ? map[i].event : {};
				data_object.class = (map[i].classes && map[i].classes[j] ? map[i].classes[j]: "") 
							+ (map[i].class ? (" " + map[i].class) : "");
				data_object.fill = map[i].fills && map[i].fills[j] ? map[i].fills[j] : map[i].fill;
				data_object.dom_attributes = map[i].dom_attribute ? map[i].dom_attribute : {};

				if(map[i].transforms){
					for(var key in map[i].transforms[j]){
						data_object.transform[key] = map[i].transforms[j][key];
					}
				}
				if(map[i].events){
					for(var key in map[i].events[j]){
						data_object.events[key] = map[i].events[j][key];
					}
				}
				if(map[i].dom_attributes){
					for(var key in map[i].dom_attributes[j]){
						data_object.dom_attributes[key] = map[i].dom_attributes[j][key];
					}
				}

				data.push(data_object);
			}
		}

		return data;
	}

	var $verifyMapElementFormat = function(map_element){
		var t_len = map_element.transforms ? map_element.transforms.length : -1,
			e_len = map_element.events ? map_element.events.length : -1,
			c_len = map_element.classes ? map_element.classes.length: -1,
			f_len = map_element.fills ? map_element.fills.length: -1,
			d_len = map_element.dom_attributes ? map_element.dom_attributes.length: -1,
			match_len = t_len > -1 ? t_len : (e_len > -1 ? e_len: (c_len > -1? c_len: (f_len > -1? f_len: d_len)));

		if(match_len == -1) return true;
		else if((t_len > 1 && t_len != match_len)
				|| (e_len > 1 && e_len != match_len)
				|| (c_len > 1 && c_len != match_len)
				|| (f_len > 1 && f_len != match_len)
				|| (d_len > 1 && d_len != match_len)) {
			console.warn($warning.improperMapData);
			return false;
		}
		else return true;

	}

	var $render = function(){
		var xmlns = "http://www.w3.org/2000/svg",
			wrapper = document.getElementById($.wrapper_id),
			svg = document.createElementNS (xmlns, "svg"),
			g,
			plan = document.createDocumentFragment();
		
		svg.setAttributeNS(null, "class", "layout");

		for(var i = 0, len = $.data.length; i < len; i++){
			g = document.createElementNS(xmlns, "g");

			if($.data[i].transform){
				g.setAttributeNS(null, "transform", 
					($.data[i].transform.translate ? ("translate(" + ($.data[i].transform.translate.x ? $.data[i].transform.translate.x : 0) + "," + ($.data[i].transform.translate.y ? $.data[i].transform.translate.y : 0) + ")") : "")
					+ " "
					+ ($.data[i].transform.scale ? ("scale(" + ($.data[i].transform.scale.x ? $.data[i].transform.scale.x : 0) + "," + ($.data[i].transform.scale.y ? $.data[i].transform.scale.y : 0) + ")") : "")
					+ " "
					+ ($.data[i].transform.rotate ? ("rotate(" + ($.data[i].transform.rotate.x ? $.data[i].transform.rotate.x : 0) + "," + ($.data[i].transform.rotate.y ? $.data[i].transform.rotate.y : 0) + "," + ($.data[i].transform.rotate.z ? $.data[i].transform.rotate.z : 0) + ")") : "")
					);
			}

			$.data[i].fill ? g.setAttributeNS(null, "fill", $.data[i].color) : undefined;
			$.data[i].class ? g.setAttributeNS(null, "class", $.data[i].class) : undefined;

			$.data[i].events && $.data[i].events.click ? g.onclick = $.data[i].events.click : undefined;
			$.data[i].events && $.data[i].events.hover ? g.onhover = $.data[i].events.hover : undefined;

			//Custom Attributes
			for(var j in $.data[i].dom_attributes){
				g.setAttributeNS(null, j, $.data[i].dom_attributes[j]);
			}

			g.innerHTML = $.data[i].model;
			plan.appendChild(g);
		}

		svg.appendChild(plan);
		wrapper.appendChild(svg);
	}

	var $autoScale = function(){

	}

	return Layout;
})();