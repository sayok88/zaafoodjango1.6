from django.conf.urls import *
from rest_auth.registration.views import SocialLoginView
from django.contrib import admin


from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter

urlpatterns = [
    # Examples:
    # url(r'^$', 'zaafoo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^', include('zaafoomain.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^rest-auth/facebook/$', FacebookLogin.as_view(), name='fb_login'),
    url(r'^gateway/', include('ebs_gateway.urls'))
]
